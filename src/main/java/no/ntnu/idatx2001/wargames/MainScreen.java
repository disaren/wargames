package no.ntnu.idatx2001.wargames;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatx2001.wargames.objects.ListOfArmy;

import java.io.IOException;

/**
 * Represents the first scene that is displayed
 * in the application, with logo and background
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class MainScreen extends Application {
    private int WIDTH = 800;
    private int HEIGHT = 800;
    private ListOfArmy listOfArmy;

    /**
     * Starts the application with the specified stage
     *
     * @param stage    The stage in which the scenes will be displayed
     */
    @Override
    public void start(Stage stage) {

        listOfArmy = new ListOfArmy();

        Text titleTop = new Text("Battle");
        Text titleMiddle = new Text("of the");
        Text titleBottom = new Text("Legions");

        Button playButton = new Button("Play");

        VBox titleBox = new VBox(titleTop,titleMiddle,titleBottom);

        VBox layout = new VBox(titleBox,playButton);

        layout.setId("layout");
        titleTop.setId("title");
        titleMiddle.setId("titleMiddle");
        titleBottom.setId("title");
        titleBox.setId("titleBox");


        playButton.setOnAction(actionEvent -> {
            SceneController sceneController = new SceneController(stage, listOfArmy);
            sceneController.switchScenes(9);
        });


        Scene scene = new Scene(layout,WIDTH,HEIGHT);
        scene.getStylesheets().add(getClass().getResource("MainScreen.css").toExternalForm());

        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }
}