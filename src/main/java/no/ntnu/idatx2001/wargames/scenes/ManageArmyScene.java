package no.ntnu.idatx2001.wargames.scenes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatx2001.wargames.SceneController;
import no.ntnu.idatx2001.wargames.exceptions.CommaInNameException;
import no.ntnu.idatx2001.wargames.exceptions.FileNotCsvException;
import no.ntnu.idatx2001.wargames.objects.Army;
import no.ntnu.idatx2001.wargames.objects.ListOfArmy;
import no.ntnu.idatx2001.wargames.objects.TerrainType;
import no.ntnu.idatx2001.wargames.objects.data.DataController;
import no.ntnu.idatx2001.wargames.objects.units.Unit;
import no.ntnu.idatx2001.wargames.objects.units.UnitFactory;
import no.ntnu.idatx2001.wargames.objects.units.UnitType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents the screen where you are able to
 * manage all the imported armies. Here you have
 * the ability to view all the units within each army.
 * You are also able to create a new army from this screen.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class ManageArmyScene {
    private Scene scene;
    private int WIDTH = 800;
    private int HEIGHT = 800;

    private ListOfArmy listOfArmy;

    /**
     * Constructor for the ManageArmyScene
     *
     * @param stage             The stage in which the scene is displayed
     * @param listOfArmy        The list of armies registered
     */
    public ManageArmyScene(Stage stage, ListOfArmy listOfArmy){
        this.listOfArmy = listOfArmy;

        ObservableList<Army> armyObservableList = FXCollections.observableArrayList();
        armyObservableList.addAll(listOfArmy.getArmyList());
        ListView<Army> armyListView = new ListView<>(armyObservableList);

        Label placeHolder = new Label("No armies imported");
        placeHolder.setFont(new Font(15));
        placeHolder.setTextFill(Paint.valueOf("white"));
        armyListView.setPlaceholder(placeHolder);

        ObservableList<Army> searchedObservableList = FXCollections.observableArrayList();

        Label title = new Label("Manage Armies");
        Button returnButton = new Button("X");
        TextField searchBar = new TextField();
        Button createArmyButton = new Button("Create new army");
        Button editArmyButton = new Button("Edit Army");

        TableView<Unit> tableView = new TableView<>();
        TableColumn unitTypeCol = new TableColumn("Unit Type");
        TableColumn unitNameCol = new TableColumn("Name");
        TableColumn unitHealthCol = new TableColumn("Health");
        tableView.getColumns().addAll(unitTypeCol,unitNameCol,unitHealthCol);
        tableView.setEditable(false);
        tableView.setPlaceholder(new Label("Select army from list"));

        unitTypeCol.setMinWidth(150);
        unitNameCol.setMinWidth(150);
        unitHealthCol.setMinWidth(150);


        unitTypeCol.setCellValueFactory(
                new PropertyValueFactory<Unit, UnitType>("UnitType")
        );
        unitNameCol.setCellValueFactory(
                new PropertyValueFactory<Unit, String>("name")
        );
        unitHealthCol.setCellValueFactory(
                new PropertyValueFactory<Unit, Integer>("health")
        );


        VBox leftCenterScreen = new VBox(searchBar,armyListView);
        HBox topScreen = new HBox(title,returnButton);
        HBox centerScreen = new HBox(leftCenterScreen,tableView);
        HBox bottomScreen = new HBox(createArmyButton,editArmyButton);


        VBox layout = new VBox(topScreen,centerScreen,bottomScreen);

        layout.setId("layout");
        topScreen.setId("topScreen");
        leftCenterScreen.setId("leftCenterScreen");
        centerScreen.setId("centerScreen");
        title.setId("title");
        returnButton.setId("returnButton");
        createArmyButton.setId("createArmyButton");
        editArmyButton.setId("editArmyButton");
        bottomScreen.setId("bottomScreen");


        returnButton.setOnAction(actionEvent -> {
            SceneController sceneController = new SceneController(stage, listOfArmy);
            sceneController.switchScenes(9);
        });

        armyListView.getSelectionModel().selectedItemProperty().addListener((observable,s,selectedArmy) -> {
            if(selectedArmy != null){
                ObservableList<Unit> unitObservableList = FXCollections.observableList(selectedArmy.getAllUnits());
                tableView.setItems(unitObservableList);


                editArmyButton.setOnAction(actionEvent -> {
                    final Stage editArmyPopup = new Stage();
                    editArmyPopup.initModality(Modality.APPLICATION_MODAL);
                    editArmyPopup.initOwner(stage);

                    VBox unitVBox = new VBox();

                    for (Unit unit: selectedArmy.getAllUnits()) {
                        HBox unitRow = new HBox();
                        Label unitTypeLabel = new Label(unit.getUnitType().toString());
                        unitTypeLabel.setMinWidth(100);
                        unitRow.getChildren().add(unitTypeLabel);

                        TextField unitName = new TextField(unit.getName());
                        unitRow.getChildren().add(unitName);

                        TextField unitHealth = new TextField(String.valueOf(unit.getHealth()));
                        unitRow.getChildren().add(unitHealth);

                        Button removeUnit = new Button("X");
                        unitRow.getChildren().add(removeUnit);

                        unitRow.setSpacing(10);

                        unitVBox.getChildren().add(unitRow);

                        removeUnit.setOnAction(removeEvent -> {

                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Unit Removal");
                            alert.setHeaderText(null);
                            alert.setContentText("Are you sure you want to remove this unit?");


                            alert.getButtonTypes().addAll(ButtonType.YES,ButtonType.NO);
                            alert.getButtonTypes().remove(ButtonType.OK);

                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.YES){
                                if(unitVBox.getChildren().size() > 1){
                                    unitVBox.getChildren().remove(unitRow);
                                    selectedArmy.remove(unit);
                                }else{
                                    Alert cantRemoveAlert = new Alert(Alert.AlertType.WARNING);
                                    cantRemoveAlert.setHeaderText(null);
                                    cantRemoveAlert.setContentText("Army cant be empty");
                                    cantRemoveAlert.show();
                                }
                            }
                        });

                    }

                    Button saveChanges = new Button("Save Changes");
                    Button addUnit = new Button("Add unit");

                    HBox bottomButtons = new HBox(addUnit,saveChanges);
                    bottomButtons.setSpacing(10);


                    addUnit.setOnAction(addUnitEvent -> {
                        final Stage addUnitPopup = new Stage();
                        addUnitPopup.initModality(Modality.APPLICATION_MODAL);
                        addUnitPopup.initOwner(editArmyPopup);

                        HBox newUnitRow = new HBox();
                        ComboBox<UnitType> unitTypeComboBox = new ComboBox<UnitType>(FXCollections.observableArrayList(UnitType.values()));
                        newUnitRow.getChildren().add(unitTypeComboBox);

                        TextField newUnitNameTextField = new TextField();
                        TextField newUnitHealthTextField = new TextField();
                        newUnitNameTextField.setPromptText("Unit Name");
                        newUnitHealthTextField.setPromptText("Unit Health");

                        newUnitRow.getChildren().add(newUnitNameTextField);
                        newUnitRow.getChildren().add(newUnitHealthTextField);

                        Button addNewUnitButton = new Button("Add Unit");

                        addNewUnitButton.setOnAction(addNewUnitEvent -> {
                            if(unitTypeComboBox.getValue() != null){

                                try{
                                    UnitFactory unitFactory = new UnitFactory();
                                    UnitType newUnitType = unitTypeComboBox.getValue();
                                    String newUnitName = newUnitNameTextField.getText();
                                    if(newUnitName.isEmpty()){
                                        throw new NullPointerException("Input a name");
                                    }
                                    Integer newUnitHealth = Integer.parseInt(newUnitHealthTextField.getText());

                                    Unit newUnit = unitFactory.createUnit(newUnitType,newUnitName,newUnitHealth);
                                    selectedArmy.add(newUnit);

                                    HBox unitRow = new HBox();
                                    Label unitTypeLabel = new Label(newUnit.getUnitType().toString());
                                    unitTypeLabel.setMinWidth(100);
                                    unitRow.getChildren().add(unitTypeLabel);

                                    TextField unitNameTextField = new TextField(newUnit.getName());
                                    unitRow.getChildren().add(unitNameTextField);

                                    TextField unitHealthTextField = new TextField(String.valueOf(newUnit.getHealth()));
                                    unitRow.getChildren().add(unitHealthTextField);

                                    Button removeUnit = new Button("X");
                                    unitRow.getChildren().add(removeUnit);

                                    unitRow.setSpacing(10);

                                    unitVBox.getChildren().add(unitRow);
                                    removeUnit.setOnAction(removeEvent -> {

                                        Alert alert = new Alert(Alert.AlertType.WARNING);
                                        alert.setTitle("Unit Removal");
                                        alert.setHeaderText(null);
                                        alert.setContentText("Are you sure you want to remove this unit?");


                                        alert.getButtonTypes().addAll(ButtonType.YES,ButtonType.NO);
                                        alert.getButtonTypes().remove(ButtonType.OK);

                                        Optional<ButtonType> result = alert.showAndWait();
                                        if (result.get() == ButtonType.YES){
                                            if(unitVBox.getChildren().size() > 1){
                                                unitVBox.getChildren().remove(unitRow);
                                                selectedArmy.remove(newUnit);
                                            }else{
                                                Alert cantRemoveAlert = new Alert(Alert.AlertType.WARNING);
                                                cantRemoveAlert.setHeaderText(null);
                                                cantRemoveAlert.setContentText("Army cant be empty");
                                                cantRemoveAlert.show();
                                            }
                                        }
                                    });
                                    addUnitPopup.close();

                                } catch (NumberFormatException nfe){
                                    Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                    errorAlert.setHeaderText(null);
                                    errorAlert.setContentText("Input integer for health");
                                    errorAlert.show();
                                } catch (NullPointerException | CommaInNameException e){
                                    Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                    errorAlert.setHeaderText(null);
                                    errorAlert.setContentText(e.getMessage());
                                    errorAlert.show();
                                }
                            }else{
                                Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                errorAlert.setHeaderText(null);
                                errorAlert.setContentText("Select unit type");
                                errorAlert.show();
                            }

                        });



                        VBox addUnitPopupLayout = new VBox(newUnitRow,addNewUnitButton);
                        addUnitPopupLayout.setSpacing(20);
                        Scene addUnitPopupScene = new Scene(addUnitPopupLayout,600,80);
                        addUnitPopup.setScene(addUnitPopupScene);
                        addUnitPopup.show();

                    });

                    TextField armyName = new TextField(selectedArmy.getName());

                    saveChanges.setOnAction(saveChangesEvent -> {

                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setHeaderText(null);
                        alert.setContentText("Are you sure you want to save changes");

                        alert.getButtonTypes().add(ButtonType.YES);
                        alert.getButtonTypes().remove(ButtonType.OK);

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.YES){

                            ArrayList<Unit> unitArrayList = new ArrayList<>();
                            ObservableList<Node> hBoxObservableList = unitVBox.getChildren();
                            UnitFactory unitFactory = new UnitFactory();
                            boolean noError = true;

                            if(armyName.getText().contains(",")){
                                Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                errorAlert.setHeaderText(null);
                                errorAlert.setContentText("Army name cant contain a comma");
                                errorAlert.show();
                                noError = false;
                            }

                            boolean armyNameAlreadyExists = false;
                            for (Army army: listOfArmy.getArmyList()) {
                                if(army != selectedArmy && army.getName().equals(armyName.getText())){
                                    armyNameAlreadyExists = true;
                                }
                            }

                            for (Node row: hBoxObservableList) {
                                HBox hBox = (HBox) row;
                                Label unitTypeLabel = (Label) hBox.getChildren().get(0);
                                TextField unitNameTextField = (TextField) hBox.getChildren().get(1);
                                TextField unitHealthTextField = (TextField) hBox.getChildren().get(2);
                                try{
                                    if(unitNameTextField.getText().isEmpty()){
                                        throw new NullPointerException("Input name for unit");
                                    }
                                    unitArrayList.add(
                                            unitFactory.createUnit(UnitType.valueOf(unitTypeLabel.getText()),
                                                    unitNameTextField.getText(),
                                                    Integer.parseInt(unitHealthTextField.getText()))
                                    );
                                }catch (NumberFormatException nfe){
                                    Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                    errorAlert.setHeaderText(null);
                                    errorAlert.setContentText("Input numbers for health value");
                                    errorAlert.show();
                                    noError = false;
                                    break;
                                }catch (NullPointerException | CommaInNameException e){
                                    Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                    errorAlert.setHeaderText(null);
                                    errorAlert.setContentText(e.getMessage());
                                    errorAlert.show();
                                    noError = false;
                                    break;
                                }
                            }

                            if(noError && !armyNameAlreadyExists){
                                selectedArmy.getAllUnits().clear();
                                selectedArmy.addAll(unitArrayList);
                                boolean newArmyName = !selectedArmy.getName().equals(armyName.getText());
                                String oldArmyName = selectedArmy.getName();

                                selectedArmy.setName(armyName.getText());

                                try{
                                    File file = new File("src\\main\\resources\\no\\ntnu\\idatx2001\\wargames\\data\\" + selectedArmy.getName() + ".csv");

                                    if(file.exists()){
                                        DataController dataController = new DataController(file);
                                        dataController.writeArmyToFile(selectedArmy);

                                        Alert armyOverwriteAlert = new Alert(Alert.AlertType.INFORMATION);
                                        armyOverwriteAlert.setHeaderText(null);
                                        armyOverwriteAlert.setContentText("Army file has been overwritten\n\n" + file.getAbsolutePath());
                                        armyOverwriteAlert.showAndWait();
                                    }else{

                                        if(newArmyName){
                                            File fileWithOldName = new File("src\\main\\resources\\no\\ntnu\\idatx2001\\wargames\\data\\" + oldArmyName + ".csv");
                                            boolean rename = fileWithOldName.renameTo(file);

                                            if(rename){
                                                DataController dataController = new DataController(file);
                                                dataController.writeArmyToFile(selectedArmy);

                                                Alert armyOverwriteAlert = new Alert(Alert.AlertType.INFORMATION);
                                                armyOverwriteAlert.setHeaderText(null);
                                                armyOverwriteAlert.setContentText("Army file has been overwritten\n\n" + file.getAbsolutePath());
                                                armyOverwriteAlert.showAndWait();
                                            }else{
                                                DataController dataController = new DataController(file);
                                                dataController.writeArmyToFile(selectedArmy);

                                                Alert armyOverwriteAlert = new Alert(Alert.AlertType.INFORMATION);
                                                armyOverwriteAlert.setHeaderText(null);
                                                armyOverwriteAlert.setContentText("Army file name does not correspond with army name\n" +
                                                                                  "Created new file with army name as file name\n\n" + file.getAbsolutePath());
                                                armyOverwriteAlert.showAndWait();
                                            }

                                        }else{
                                            DataController dataController = new DataController(file);
                                            dataController.writeArmyToFile(selectedArmy);

                                            throw new FileNotFoundException("Original army file did not originate from the default directory" +
                                                    "\n\nCreated a new army file copy in: \n"
                                                    + file.getAbsolutePath());
                                        }

                                    }
                                } catch (FileNotFoundException | FileNotCsvException exception){
                                    Alert errorAlert = new Alert(Alert.AlertType.INFORMATION);
                                    errorAlert.setHeaderText(null);
                                    errorAlert.setContentText(exception.getMessage());
                                    errorAlert.showAndWait();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                editArmyPopup.close();
                                SceneController sceneController = new SceneController(stage, listOfArmy);
                                sceneController.switchScenes(3);
                            }else if(armyNameAlreadyExists){
                                Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                                errorAlert.setHeaderText(null);
                                errorAlert.setContentText("Army name already exists");
                                errorAlert.showAndWait();
                            }
                        }

                    });

                    ScrollPane scrollPane = new ScrollPane(unitVBox);
                    unitVBox.setSpacing(3);

                    armyName.setMinWidth(200);

                    VBox editArmyPopupLayout = new VBox(scrollPane,armyName,bottomButtons);
                    editArmyPopupLayout.setSpacing(10);

                    Scene editArmyPopupScene = new Scene(editArmyPopupLayout,600,400);
                    editArmyPopup.setScene(editArmyPopupScene);
                    editArmyPopup.show();
                });

            }else{
                tableView.setItems(null);
                editArmyButton.setOnAction(null);
            }
        });

        searchBar.textProperty().addListener((observable,oldValue,newValue) -> {
            while(!searchedObservableList.isEmpty()){
                searchedObservableList.remove(0);
            }
            armyListView.setPlaceholder(null);

            Pattern pattern = Pattern.compile(newValue, Pattern.CASE_INSENSITIVE);


            for (Army army: armyObservableList) {
                Matcher matcher = pattern.matcher(army.toString());
                if(matcher.find()){
                    searchedObservableList.add(army);
                }
            }
            armyListView.setItems(searchedObservableList);

        });

        createArmyButton.setOnAction(actionEvent -> {
            final Stage createArmyPopup = new Stage();
            createArmyPopup.initModality(Modality.APPLICATION_MODAL);
            createArmyPopup.initOwner(stage);

            GridPane createArmyPopupLayout = new GridPane();

            Label armyNameLabel = new Label("Army Name: ");
            TextField armyNameInput = new TextField();
            HBox armyNameHBox = new HBox(armyNameLabel,armyNameInput);
            armyNameHBox.setSpacing(50);
            armyNameLabel.setMinWidth(100);

            createArmyPopupLayout.addRow(0,armyNameHBox);

            for (int i = 0; i < UnitType.values().length; i++) {
                HBox newRow = new HBox();
                Label label = new Label(UnitType.values()[i].toString());
                TextField name = new TextField();
                TextField health = new TextField();
                Spinner<Integer> numberOfUnits = new Spinner<>();
                numberOfUnits.setId("spinner");
                numberOfUnits.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,10000,0));

                numberOfUnits.setEditable(true);

                numberOfUnits.getEditor().textProperty().addListener((observable,oldValue,newValue) -> {
                    try{
                        if(numberOfUnits.getEditor().getText() != null){
                            int testNumber = Integer.parseInt(numberOfUnits.getEditor().getText());
                        }
                    }catch (NumberFormatException nfe){
                        numberOfUnits.getEditor().setText(null);
                    }
                });

                name.setPromptText("Unit Name");
                health.setPromptText("Unit Health");
                numberOfUnits.setPromptText("Number Of Units");


                newRow.getChildren().addAll(label,name,health,numberOfUnits);
                newRow.setAlignment(Pos.CENTER);
                newRow.setSpacing(50);
                label.setMinWidth(100);

                newRow.getStyleClass().add("popupRow");

                createArmyPopupLayout.addRow(i+1,newRow);
            }

            Button createArmyPopupButton = new Button("Create Army");

            createArmyPopupLayout.setVgap(20);
            createArmyPopupLayout.setHgap(15);
            createArmyPopupLayout.setAlignment(Pos.CENTER);



            createArmyPopupButton.setOnAction(createArmy -> {
                ObservableList<Node> nodeObservableList = createArmyPopupLayout.getChildren();
                HBox firstHBox = (HBox) nodeObservableList.get(0);
                TextField newArmyName = (TextField) firstHBox.getChildren().get(1);

                if(!newArmyName.getText().isEmpty() && !newArmyName.getText().contains(",")){
                    boolean error = false;
                    Army newArmy = new Army(newArmyName.getText());

                    for (int i = 1; i < nodeObservableList.size() - 1; i++) {
                        HBox unitHBox = (HBox) nodeObservableList.get(i);
                        Label newUnitTypeNode = (Label) unitHBox.getChildren().get(0);
                        TextField newUnitNameNode = (TextField) unitHBox.getChildren().get(1);
                        TextField newUnitHealthNode = (TextField) unitHBox.getChildren().get(2);
                        Spinner<Integer> amountOfUnitsNode = (Spinner<Integer>) unitHBox.getChildren().get(3);

                        if(amountOfUnitsNode.getValue() == null && !newUnitNameNode.getText().isEmpty()){
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setHeaderText(null);
                            alert.setContentText("Input number for amount of units");
                            alert.show();
                            error = true;
                            break;
                        }

                        if(amountOfUnitsNode.getValue() != null && amountOfUnitsNode.getValue() > 0){
                            try{
                                if(newUnitHealthNode.getText() != null){
                                    int newUnitHealth = Integer.parseInt(newUnitHealthNode.getText());
                                    UnitType newUnitType = UnitType.valueOf(newUnitTypeNode.getText());
                                    int amountOfUnits = amountOfUnitsNode.getValue();
                                    String newUnitName;
                                    if(!newUnitNameNode.getText().isEmpty()){
                                        newUnitName = newUnitNameNode.getText();
                                    }else{
                                        throw new IllegalArgumentException("Need to input unit name");
                                    }

                                    newArmy.addAll(new UnitFactory().createUnitList(amountOfUnits,newUnitType,newUnitName,newUnitHealth));
                                }else{
                                    throw new NullPointerException("Input unit health");
                                }


                            }catch (NumberFormatException nfe){
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.setHeaderText(null);
                                alert.setContentText("Can only input numbers in health field");
                                alert.show();
                                error = true;
                                break;
                            }catch (IllegalArgumentException iae){
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.setHeaderText(null);
                                alert.setContentText("Need to input unit name");
                                alert.show();
                                error = true;
                                break;
                            }catch (CommaInNameException commaInNameException){
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.setHeaderText(null);
                                alert.setContentText("Unit name cannot contain a comma");
                                alert.show();
                                error = true;
                                break;
                            }
                        }
                    }

                    if(!error){
                        try{
                            if(!newArmy.getAllUnits().isEmpty()){
                                DataController dataController = new DataController(newArmy.getName());
                                String dataNewArmyName = dataController.getFileName().substring(dataController.getFileName().lastIndexOf("\\") + 1);
                                newArmy.setName(dataNewArmyName);
                                dataController.writeArmyToFile(newArmy);
                                listOfArmy.addArmy(newArmy);
                                armyObservableList.add(newArmy);
                                createArmyPopup.close();
                            }else{
                                throw new Exception("No units in army");
                            }


                        } catch (Exception e) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setHeaderText(null);
                            alert.setContentText(e.getMessage());
                            alert.show();
                        }
                    }


                }else if(newArmyName.getText().contains(",")){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setContentText("Army name cant contain a comma");
                    alert.show();
                }else{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setContentText("Input name for army");
                    alert.show();
                }



            });

            createArmyPopupLayout.setId("createArmyPopupLayout");

            ScrollPane scrollPane = new ScrollPane(createArmyPopupLayout);

            VBox createArmyPopupVBox = new VBox(scrollPane,createArmyPopupButton);
            createArmyPopupVBox.setId("createArmyPopupLayout");

            Scene createArmyPopupScene = new Scene(createArmyPopupVBox,570,360);
            createArmyPopupScene.getStylesheets().add(getClass().getResource("ManageArmyScene.css").toExternalForm());
            createArmyPopup.setScene(createArmyPopupScene);
            createArmyPopup.show();
        });


        scene = new Scene(layout,WIDTH,HEIGHT);
        scene.getStylesheets().add(getClass().getResource("ManageArmyScene.css").toExternalForm());

        stage.setScene(scene);
        stage.show();

    }

    /**
     * Returns the scene containing all the nodes
     *
     * @return Scene with all the nodes
     */
    public Scene getScene() {
        return scene;
    }
}
