package no.ntnu.idatx2001.wargames.scenes;

import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import no.ntnu.idatx2001.wargames.SceneController;
import no.ntnu.idatx2001.wargames.objects.Army;
import no.ntnu.idatx2001.wargames.objects.Battle;
import no.ntnu.idatx2001.wargames.objects.ListOfArmy;
import no.ntnu.idatx2001.wargames.objects.TerrainType;
import no.ntnu.idatx2001.wargames.objects.units.Unit;
import no.ntnu.idatx2001.wargames.objects.units.UnitFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Represents the simulation screen for simulating
 * a battle between two armies. The units are drawn
 * to the screen and engage visually in a battle
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class SimulateScene {
    private Scene scene;
    private int WIDTH = 800;
    private int HEIGHT = 800;

    private ListOfArmy listOfArmy;
    private TerrainType terrainType;
    private ListOfArmy selectedArmyList;

    private Battle battle;

    /**
     * Constructor for the SimulateScene
     *
     * @param stage             The stage in which the scene is displayed
     * @param listOfArmy        The list of armies registered
     * @param selectedArmyList  The list of armies that have been selected to fight each other
     * @param terrainType       The terrain type selected for fighting
     */
    public SimulateScene(Stage stage, ListOfArmy listOfArmy, ListOfArmy selectedArmyList,TerrainType terrainType){
        this.listOfArmy = listOfArmy;
        this.terrainType = terrainType;
        this.selectedArmyList = selectedArmyList;

        ArrayList<Unit> unitListCopyOne = new ArrayList<>();
        ArrayList<Unit> unitListCopyTwo = new ArrayList<>();
        UnitFactory unitFactory = new UnitFactory();

        for (Unit unit: selectedArmyList.getArmyList().get(0).getAllUnits()) {
            Unit newUnit = unitFactory.createUnit(unit.getUnitType(),unit.getName(),unit.getHealth());
            unitListCopyOne.add(newUnit);
        }
        for (Unit unit: selectedArmyList.getArmyList().get(1).getAllUnits()) {
            Unit newUnit = unitFactory.createUnit(unit.getUnitType(),unit.getName(),unit.getHealth());
            unitListCopyTwo.add(newUnit);
        }
        Army selectedArmyCopyOne = new Army(selectedArmyList.getArmyList().get(0).getName(),unitListCopyOne);
        Army selectedArmyCopyTwo = new Army(selectedArmyList.getArmyList().get(1).getName(),unitListCopyTwo);

        battle = new Battle(selectedArmyCopyOne, selectedArmyCopyTwo, terrainType);

        ArrayList<Circle> unitCirclesBlue = new ArrayList<>();
        ArrayList<Circle> unitCirclesRed = new ArrayList<>();

        Label title = new Label("Battle Field");
        Button returnButton = new Button("X");

        Label armyOneLabel = new Label(selectedArmyCopyOne.getName());
        Label armyTwoLabel = new Label(selectedArmyCopyTwo.getName());
        Button battleButton = new Button("Battle");

        Pane leftPane = new Pane();
        Pane rightPane = new Pane();

        TranslateTransition leftTT = new TranslateTransition(Duration.millis(2000), leftPane);
        TranslateTransition rightTT = new TranslateTransition(Duration.millis(2000), rightPane);

        Label winnerLabel = new Label();
        Label battleMessage = new Label();
        Label terrainLabel = new Label(terrainType.toString());

        HBox speedUpButtons = new HBox();

        Button resetButton = new Button();
        resetButton.setOnAction(null);

        for (int i = 0; i < 5; i++) {
            Button speedUpButton = new Button(i+1 + "x");

            int finalI = i + 1;
            speedUpButton.setOnAction(actionEvent -> {
                battle.setSleepTime(1000 / finalI);
            });

            speedUpButtons.getChildren().add(speedUpButton);
            speedUpButton.getStyleClass().add("speedUpButton");
        }

        Button speedUpButton10 = new Button("10x");
        speedUpButtons.getChildren().add(speedUpButton10);
        speedUpButton10.getStyleClass().add("speedUpButton");

        speedUpButton10.setOnAction(actionEvent -> {
            battle.setSleepTime(100);
        });

        Button speedUpButton100 = new Button("100x");
        speedUpButtons.getChildren().add(speedUpButton100);
        speedUpButton100.getStyleClass().add("speedUpButton");

        speedUpButton100.setOnAction(actionEvent -> {
            battle.setSleepTime(10);
        });

        Button speedUpButton1000 = new Button("1000x");
        speedUpButtons.getChildren().add(speedUpButton1000);
        speedUpButton1000.getStyleClass().add("speedUpButton");

        speedUpButton1000.setOnAction(actionEvent -> {
            battle.setSleepTime(1);
        });

        VBox bottomMiddle = new VBox(winnerLabel);

        HBox topScreen = new HBox(title,returnButton);
        HBox betweenTopCenterRow = new HBox(armyOneLabel,battleButton,armyTwoLabel);
        HBox centerScreen = new HBox(leftPane,rightPane);
        HBox bottomScreen = new HBox(battleMessage,bottomMiddle,terrainLabel);



        VBox layout = new VBox(topScreen,betweenTopCenterRow,centerScreen,bottomScreen,speedUpButtons);

        layout.setId("layout");
        topScreen.setId("topScreen");
        title.setId("title");
        returnButton.setId("returnButton");
        centerScreen.setId("centerScreen");
        betweenTopCenterRow.setId("betweenTopCenterRow");
        bottomScreen.setId("bottomScreen");
        battleMessage.setId("battleMessage");
        winnerLabel.setId("winnerLabel");
        terrainLabel.setId("terrainLabel");
        speedUpButtons.setId("speedUpButtons");
        bottomMiddle.setId("bottomMiddle");


        leftPane.getStyleClass().add("pane");
        rightPane.getStyleClass().add("pane");


        returnButton.setOnAction(actionEvent -> {
            SceneController sceneController = new SceneController(stage, listOfArmy);
            sceneController.switchScenes(1);
        });


        for (Unit ignored : selectedArmyCopyOne.getAllUnits()) {
            Random rand = new Random();
            double randomCoordsX = (rand.nextDouble()*99) + 100;
            double randomCoordsY = (rand.nextDouble()*199) + 100;


            Circle unitCircle = new Circle(randomCoordsX,randomCoordsY,3);
            unitCircle.setFill(Paint.valueOf("blue"));
            leftPane.getChildren().add(unitCircle);
            unitCirclesBlue.add(unitCircle);
        }

        for (Unit ignored : selectedArmyCopyTwo.getAllUnits()) {
            Random rand = new Random();
            double randomCoordsX = (rand.nextDouble()*99) + 100;
            double randomCoordsY = (rand.nextDouble()*199) + 100;


            Circle unitCircle = new Circle(randomCoordsX,randomCoordsY,3);
            unitCircle.setFill(Paint.valueOf("red"));
            rightPane.getChildren().add(unitCircle);
            unitCirclesRed.add(unitCircle);
        }


        battleButton.setOnAction(actionEvent -> {

            battleButton.setOnAction(null);

            leftTT.setByX(130);
            rightTT.setByX(-130);

            leftTT.play();
            rightTT.play();


            leftTT.setOnFinished(animationEvent -> {
                animateWar(unitCirclesBlue, 1);
                animateWar(unitCirclesRed, -1);
            });


            Thread battleThread = new Thread(() -> {
                Army winnerArmy = null;
                try {
                    winnerArmy = battle.simulate();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Army finalWinnerArmy = winnerArmy;
                Platform.runLater(() -> {
                    if(finalWinnerArmy != null){
                        winnerLabel.setText("WINNER:\n" + finalWinnerArmy.getName());
                        resetButton.setText("Reset");
                        bottomMiddle.getChildren().add(resetButton);
                        resetButton.setOnAction(resetEvent -> {
                            SceneController sceneController = new SceneController(stage, listOfArmy, selectedArmyList,terrainType);
                            sceneController.switchScenes(2);
                        });
                    }
                });
            });

            Thread messageThread = new Thread(() -> {
               while(battleThread.isAlive() && battle.getAttackerOne() != null){

                   String message = battle.getAttackerOne().getName() +
                                    " and " + battle.getAttackerTwo().getName() +
                                    "\nare attacking each other" +
                                    "\n\n" + battle.getAttackerOne().getName() + " now has: " + battle.getAttackerOne().getHealth() + "HP" +
                                    "\n" + battle.getAttackerTwo().getName() + " now has: " + battle.getAttackerTwo().getHealth() + "HP";

                   Platform.runLater(() ->{
                       battleMessage.setText(message);
                       if(battle.getAttackerOne() != null && battle.getAttackerOne().getHealth() == 0){
                               Random rand = new Random();

                               if(unitCirclesBlue.size() >= 1){
                               int randomInt = rand.nextInt(unitCirclesBlue.size());

                               leftPane.getChildren().remove(unitCirclesBlue.get(randomInt));
                               unitCirclesBlue.remove(randomInt);
                           }


                           }
                       if(battle.getAttackerTwo() != null && battle.getAttackerTwo().getHealth() == 0){
                               Random rand = new Random();
                               if(unitCirclesRed.size() >= 1){
                                   int randomInt = rand.nextInt(unitCirclesRed.size());

                                   rightPane.getChildren().remove(unitCirclesRed.get(randomInt));
                                   unitCirclesRed.remove(randomInt);
                               }
                           }

                   });

                   try {
                       Thread.sleep(battle.getSleepTime());
                   } catch (InterruptedException ie) {
                       ie.printStackTrace();
                   }
               }
               if(unitCirclesBlue.size() > unitCirclesRed.size()){
                   for (Circle circle: unitCirclesRed) {
                       Platform.runLater(() -> {
                           rightPane.getChildren().remove(circle);
                       });
                   }
                   unitCirclesRed.clear();
               }else{
                   for (Circle circle: unitCirclesBlue) {
                       Platform.runLater(() -> {
                           leftPane.getChildren().remove(circle);
                       });
                   }
                   unitCirclesBlue.clear();
               }
            });

            battleThread.start();
            messageThread.start();



            returnButton.setOnAction(returnEvent -> {
                battleThread.interrupt();
                messageThread.interrupt();
                SceneController sceneController = new SceneController(stage, listOfArmy);
                sceneController.switchScenes(1);
            });

            stage.setOnCloseRequest(stopEvent -> {
                battleThread.interrupt();
                messageThread.interrupt();
            });

        });





        scene = new Scene(layout,WIDTH,HEIGHT);
        scene.getStylesheets().add(getClass().getResource("SimulateScene.css").toExternalForm());
        stage.setScene(scene);
        stage.show();

    }


    /**
     * Method for animating a list of circles
     *
     * @param circleList    The list of circles that should be animated
     * @param direction     A number in which the operator,
     *                      in front of the number, determines the direction
     *                      (1: to the right, -1: to the left)
     */
    private void animateWar(List<Circle> circleList, int direction){
        for (Circle unitCircle: circleList) {
            TranslateTransition firstAnimation = new TranslateTransition(Duration.millis(2000),unitCircle);
            Random rand = new Random();
            firstAnimation.setByX(rand.nextInt(100) * direction);
            firstAnimation.play();

            firstAnimation.setOnFinished(event1 -> {
                TranslateTransition yAnimation = new TranslateTransition(Duration.millis(500),unitCircle);
                int oneOrZero1 = rand.nextInt(10)%2;

                if(oneOrZero1 == 0){
                    yAnimation.setByY(rand.nextInt(10)-5);
                }else{
                    yAnimation.setByX(rand.nextInt(10)-5);
                }


                yAnimation.setCycleCount(2);
                yAnimation.setAutoReverse(true);
                yAnimation.play();

                yAnimation.setOnFinished(event2 -> {
                    TranslateTransition xAnimation = new TranslateTransition(Duration.millis(500),unitCircle);
                    int oneOrZero2 = rand.nextInt(10)%2;

                    if(oneOrZero2 == 0){
                        xAnimation.setByX(rand.nextInt(10)-5);
                    }else{
                        xAnimation.setByY(rand.nextInt(10)-5);
                    }
                    xAnimation.setByX(rand.nextInt(10)-5);
                    xAnimation.setCycleCount(2);
                    xAnimation.setAutoReverse(true);
                    xAnimation.play();

                    xAnimation.setOnFinished(event3 -> {
                        yAnimation.play();
                    });
                });
            });
        }
    }

    /**
     * Returns the scene containing all the nodes
     *
     * @return Scene with all the nodes
     */
    public Scene getScene() {
        return scene;
    }
}
