package no.ntnu.idatx2001.wargames.scenes;

import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import no.ntnu.idatx2001.wargames.SceneController;
import no.ntnu.idatx2001.wargames.exceptions.DirectoryException;
import no.ntnu.idatx2001.wargames.objects.Army;
import no.ntnu.idatx2001.wargames.objects.ListOfArmy;
import no.ntnu.idatx2001.wargames.objects.data.DataController;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the main menu screen, where you
 * are able to switch to others screens, and return
 * to this screen.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class PlayScene {
    private Scene scene;
    private int WIDTH = 800;
    private int HEIGHT = 800;

    private ListOfArmy listOfArmy;

    /**
     * Constructor for the PlayScene
     *
     * @param stage             The stage in which the scene is displayed
     * @param listOfArmy        The list of armies registered
     */
    public PlayScene(Stage stage, ListOfArmy listOfArmy){
        this.listOfArmy = listOfArmy;

        Button battle = new Button("BATTLE");
        Button manageArmies = new Button("Manage Armies");
        Button importArmy = new Button("Import Army");

        Image img = new Image(getClass().getResource("medievalwar.jpg").toExternalForm());
        BackgroundImage backgroundImage = new BackgroundImage(img,
                                                                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                                                                BackgroundPosition.CENTER,
                                                                BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        Pane backgroundImagePane = new Pane();

        Text titleTop = new Text("Battle");
        Text titleMiddle = new Text("of the");
        Text titleBottom = new Text("Legions");

        VBox titleBox = new VBox(titleTop,titleMiddle,titleBottom);

        VBox armyButtons = new VBox(manageArmies,importArmy);
        VBox layout = new VBox(armyButtons,battle);

        AnchorPane layoutWithBackground = new AnchorPane(backgroundImagePane,layout,titleBox);

        AnchorPane.setTopAnchor(backgroundImagePane, -100.0);
        AnchorPane.setLeftAnchor(backgroundImagePane, -100.0);

        AnchorPane.setLeftAnchor(layout,20.0);
        AnchorPane.setTopAnchor(layout,200.0);

        AnchorPane.setRightAnchor(titleBox, 50.0);
        AnchorPane.setTopAnchor(titleBox, 250.0);

        backgroundImagePane.setBackground(background);

        TranslateTransition backgroundAnimation = new TranslateTransition(Duration.millis(14000),backgroundImagePane);
        backgroundAnimation.setByX(-600);
        backgroundAnimation.setCycleCount(Timeline.INDEFINITE);
        backgroundAnimation.setAutoReverse(true);
        backgroundAnimation.play();


        ScaleTransition titleAnimation = new ScaleTransition(Duration.millis(1000),titleBox);
        titleAnimation.setToX(1.1);
        titleAnimation.setByY(0.1);
        titleAnimation.setAutoReverse(true);
        titleAnimation.setCycleCount(Timeline.INDEFINITE);
        titleAnimation.play();


        layout.setId("layout");
        armyButtons.setId("armyButtons");
        backgroundImagePane.setId("backgroundImagePane");
        titleTop.setId("title");
        titleMiddle.setId("titleMiddle");
        titleBottom.setId("title");
        titleBox.setId("titleBox");


        battle.setOnAction(actionEvent -> {
            SceneController sceneController = new SceneController(stage,listOfArmy);
            sceneController.switchScenes(1);
        });

        manageArmies.setOnAction(actionEvent -> {
            SceneController sceneController = new SceneController(stage,listOfArmy);
            sceneController.switchScenes(3);
        });

        importArmy.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Army File");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV, ","*.csv"),
                                                     new FileChooser.ExtensionFilter("All files", "*.*"));

            File dataDirectory = new File("src\\main\\resources\\no\\ntnu\\idatx2001\\wargames\\data\\");

            try{
                if(!dataDirectory.exists()){
                    throw new DirectoryException("Folder doesnt exist");
                }else{
                    fileChooser.setInitialDirectory(dataDirectory);
                }
            }catch (DirectoryException de){
                if(dataDirectory.mkdir()){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("Created new directory for data files" +
                            "\n" + dataDirectory.getAbsolutePath());
                    alert.showAndWait();

                    fileChooser.setInitialDirectory(dataDirectory);
                }else{
                    fileChooser.setInitialDirectory(null);
                }
            }


            List<File> files = fileChooser.showOpenMultipleDialog(stage);
            if(files != null){
                boolean armyAdded = false;
                ArrayList<File> filesImportedArrayList = new ArrayList<>();

                for (File file: files) {
                    if(file != null){
                        try{
                            DataController dataController = new DataController(file);

                            Army armyFromFile = dataController.readArmyFromFile();
                            listOfArmy.addArmy(armyFromFile);
                            armyAdded = true;
                            filesImportedArrayList.add(file);

                        }catch (Exception e){
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setHeaderText(null);
                            alert.setContentText("File selected: " + file.getName() +
                                                 "\n------------------------------\n"
                                                 + e.getMessage());
                            alert.showAndWait();

                        }
                    }
                }
                if(armyAdded){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    String contentTextString = "Valid army/armies added:\n\n";

                    for (File file: filesImportedArrayList) {
                        contentTextString += file.getName() + "\n";
                    }
                    alert.setContentText(contentTextString);
                    alert.show();
                }

            }
        });


        scene = new Scene(layoutWithBackground,WIDTH,HEIGHT);
        scene.getStylesheets().add(getClass().getResource("PlayScene.css").toExternalForm());
        stage.setScene(scene);
        stage.show();

    }

    /**
     * Returns the scene containing all the nodes
     *
     * @return Scene with all the nodes
     */
    public Scene getScene() {
        return scene;
    }
}
