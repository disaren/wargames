package no.ntnu.idatx2001.wargames.scenes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import no.ntnu.idatx2001.wargames.SceneController;
import no.ntnu.idatx2001.wargames.objects.Army;
import no.ntnu.idatx2001.wargames.objects.ListOfArmy;
import no.ntnu.idatx2001.wargames.objects.TerrainType;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents the battle screen, in which you
 * choose to armies that are gonna battle each other,
 * and the terrain for the battle.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class BattleScene {
    private Scene scene;
    private int WIDTH = 800;
    private int HEIGHT = 800;

    private ListOfArmy listOfArmy;

    /**
     * Constructor for the BattleScene
     *
     * @param stage             The stage in which the scene is displayed
     * @param listOfArmy        The list of armies registered
     */
    public BattleScene(Stage stage, ListOfArmy listOfArmy){

        this.listOfArmy = listOfArmy;
        ArrayList<Army> selectedArmyArrayList = new ArrayList<>();

        ArrayList<TerrainType> selectedTerrain = new ArrayList<>();
        selectedTerrain.add(null);

        ObservableList<Army> armyObservableList = FXCollections.observableArrayList();
        armyObservableList.addAll(listOfArmy.getArmyList());
        ListView<Army> leftArmyListView = new ListView<>(armyObservableList);
        ListView<Army> rightArmyListView = new ListView<>(armyObservableList);

        ObservableList<Army> searchedObservableList = FXCollections.observableArrayList();

        Label leftPlaceHolder = new Label("No armies imported");
        Label rightPlaceHolder = new Label("No armies imported");
        leftPlaceHolder.setFont(new Font(16));
        rightPlaceHolder.setFont(new Font(16));
        leftPlaceHolder.setTextFill(Paint.valueOf("white"));
        rightPlaceHolder.setTextFill(Paint.valueOf("white"));

        TextField searchBar = new TextField();

        leftArmyListView.setPlaceholder(leftPlaceHolder);
        rightArmyListView.setPlaceholder(rightPlaceHolder);

        Label title = new Label("Battle");
        Button returnButton = new Button("X");

        Label terrainTypeLabel = new Label();

        Button leftButton = new Button("->");
        Button rightButton = new Button("<-");
        Pane leftPane = new Pane();
        Pane rightPane = new Pane();

        HBox armyContainer = new HBox(leftPane,rightPane);


        ArrayList<Button> terrainButtonsList = new ArrayList<>();
        for (TerrainType terrainType: TerrainType.values()) {
            Button terrainButton = new Button(terrainType.name());
            terrainButtonsList.add(terrainButton);
            terrainButton.setId(terrainType.toString().toLowerCase() + "Button");
            terrainButton.getStyleClass().add("terrainButtons");

            terrainButton.setOnAction(actionEvent -> {
                selectedTerrain.set(0,terrainType);
                terrainTypeLabel.setText("Terrain selected: " + terrainType);
            });
        }
        HBox terrainButtons = new HBox();
        terrainButtons.getChildren().addAll(terrainButtonsList);

        Button battleButton = new Button("BATTLE");

        VBox middleColumn = new VBox(terrainButtons,armyContainer,battleButton);

        HBox topScreen = new HBox(title,returnButton);
        HBox centerScreen = new HBox(leftArmyListView,leftButton,middleColumn,rightButton,rightArmyListView);
        VBox layout = new VBox(topScreen,searchBar,centerScreen,terrainTypeLabel);

        layout.setId("layout");
        topScreen.setId("topScreen");
        title.setId("title");
        returnButton.setId("returnButton");
        centerScreen.setId("centerScreen");
        armyContainer.setId("armyContainer");
        middleColumn.setId("middleColumn");
        terrainButtons.setId("terrainButtonsBox");
        terrainTypeLabel.setId("terrainTypeLabel");
        battleButton.setId("battleButton");

        leftPane.getStyleClass().add("pane");
        rightPane.getStyleClass().add("pane");


        leftArmyListView.getSelectionModel().selectedItemProperty().addListener((observable,s,selectedArmy) -> {
            if(armyContainer.getChildren().get(0).getClass() != Pane.class){
                leftButton.setOnAction(null);
            }else if(selectedArmy != null){
                leftButton.setOnAction(actionEvent -> {
                    Button removeArmyButton = new Button("X");
                    HBox armySelectedBox = new HBox();
                    armySelectedBox.getChildren().add(new Label(selectedArmy.toString()));
                    armySelectedBox.getChildren().add(removeArmyButton);
                    armySelectedBox.setId("armySelectedBox");


                    removeArmyButton.setOnAction(removeEvent -> {
                        armyObservableList.add(selectedArmy);
                        searchedObservableList.add(selectedArmy);
                        armyContainer.getChildren().set(0,leftPane);
                        selectedArmyArrayList.remove(selectedArmy);
                        leftArmyListView.getSelectionModel().select(null);
                    });
                    leftButton.setOnAction(null);
                    leftArmyListView.getSelectionModel().select(null);

                    selectedArmyArrayList.add(selectedArmy);
                    armyContainer.getChildren().set(0,armySelectedBox);
                    armyObservableList.remove(selectedArmy);
                    searchedObservableList.remove(selectedArmy);

                });
                rightButton.setOnAction(null);

            }

        });

        searchBar.textProperty().addListener((observable,oldValue,newValue) -> {
            while(!searchedObservableList.isEmpty()){
                searchedObservableList.remove(0);
            }
            leftArmyListView.setPlaceholder(null);
            rightArmyListView.setPlaceholder(null);

            Pattern pattern = Pattern.compile(newValue, Pattern.CASE_INSENSITIVE);


            for (Army army: armyObservableList) {
                Matcher matcher = pattern.matcher(army.toString());
                if(matcher.find()){
                    searchedObservableList.add(army);
                }
            }
            leftArmyListView.setItems(searchedObservableList);
            rightArmyListView.setItems(searchedObservableList);

        });


        rightArmyListView.getSelectionModel().selectedItemProperty().addListener((observable,s,selectedArmy) -> {
            if(armyContainer.getChildren().get(1).getClass() != Pane.class){
                rightButton.setOnAction(null);
            }else if(selectedArmy != null){
                rightButton.setOnAction(actionEvent -> {
                    Button removeArmyButton = new Button("X");
                    HBox armySelectedBox = new HBox();
                    armySelectedBox.getChildren().add(new Label(selectedArmy.toString()));
                    armySelectedBox.getChildren().add(removeArmyButton);
                    armySelectedBox.setId("armySelectedBox");


                    removeArmyButton.setOnAction(removeEvent -> {
                        armyObservableList.add(selectedArmy);
                        searchedObservableList.add(selectedArmy);
                        armyContainer.getChildren().set(1,rightPane);
                        selectedArmyArrayList.remove(selectedArmy);
                        rightArmyListView.getSelectionModel().select(null);
                    });
                    rightButton.setOnAction(null);
                    rightArmyListView.getSelectionModel().select(null);

                    selectedArmyArrayList.add(selectedArmy);
                    armyContainer.getChildren().set(1,armySelectedBox);
                    armyObservableList.remove(selectedArmy);
                    searchedObservableList.remove(selectedArmy);

                });
                leftButton.setOnAction(null);

            }

        });

        battleButton.setOnAction(actionEvent -> {
            if(selectedArmyArrayList.size() != 2){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Select two armies");
                alert.show();
            }else if(selectedTerrain.get(0) == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Select a terrain");
                alert.show();
            }else{
                ListOfArmy selectedListOfArmy = new ListOfArmy(selectedArmyArrayList);
                SceneController sceneController = new SceneController(stage, listOfArmy, selectedListOfArmy,selectedTerrain.get(0));
                sceneController.switchScenes(2);
            }
        });


        returnButton.setOnAction(actionEvent -> {
            SceneController sceneController = new SceneController(stage, listOfArmy);
            sceneController.switchScenes(9);
        });


        scene = new Scene(layout,WIDTH,HEIGHT);
        scene.getStylesheets().add(getClass().getResource("BattleScene.css").toExternalForm());

        stage.setScene(scene);
        stage.show();

    }

    /**
     * Returns the scene containing all the nodes
     *
     * @return Scene with all the nodes
     */
    public Scene getScene() {
        return scene;
    }
}
