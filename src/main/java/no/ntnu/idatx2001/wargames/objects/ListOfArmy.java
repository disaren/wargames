package no.ntnu.idatx2001.wargames.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a list containing armies
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class ListOfArmy {
    private List<Army> armyList;

    /**
     * Constructor for a list
     */
    public ListOfArmy(){
        this.armyList = new ArrayList<>();
    }

    /**
     * Constructor for a list. Uses the list provided in
     * the parameter.
     *
     * @param armyListInput  A list containing army objects
     */
    public ListOfArmy(List<Army> armyListInput){
        this.armyList = new ArrayList<>();
        armyList.addAll(armyListInput);
    }

    /**
     * Returns the army list
     *
     * @return List of army objects
     */
    public List<Army> getArmyList() {
        return armyList;
    }

    /**
     * Adds the army into the list of armies
     *
     * @param army                          Army object that will be added to the list
     * @throws IllegalArgumentException     If army is already in the list
     */
    public void addArmy(Army army){
        if(!armyList.contains(army)){
            armyList.add(army);
        }else{
            throw new IllegalArgumentException("Army already in the list");
        }

    }

    /**
     * Adds the list to the list of armies
     *
     * @param armyListInput                 Army list that should be put in the list
     * @throws IllegalArgumentException     If armies are already in the list
     */
    public void addAllArmies(List<Army> armyListInput){
        if(!armyList.containsAll(armyListInput)){
            armyList.addAll(armyListInput);
        }else{
            throw new IllegalArgumentException("Armies already in the list");
        }

    }


    /**
     * Remove the specifed army from the army list
     *
     * @param army                          Army that should be removed from the list
     * @throws IllegalArgumentException     If army isn't in the list
     */
    public void removeArmy(Army army){
        if(armyList.contains(army)){
            armyList.remove(army);
        }else{
            throw new IllegalArgumentException("Army isnt in the list");
        }
    }

    /**
     * Removes all the armies from the list
     */
    public void removeAllArmies(){
        armyList.clear();
    }

    /**
     * Returns a standard toString string containing
     * the field variables of this object
     *
     * @return String containing the field variables
     */
    @Override
    public String toString() {
        return "ListOfArmy{" +
                "armyList=" + armyList +
                '}';
    }

    /**
     * Standard equals method, checks if the specified object
     * is equal to this object
     *
     * @param o     The object that will be checked
     * @return      true if the object is equal to this object, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListOfArmy that = (ListOfArmy) o;
        return Objects.equals(armyList, that.armyList);
    }

    /**
     *  Standard hash method, uses the army list
     *
     * @return   Hash, using the army list
     */
    @Override
    public int hashCode() {
        return Objects.hash(armyList);
    }
}
