package no.ntnu.idatx2001.wargames.objects.data;

import no.ntnu.idatx2001.wargames.exceptions.CommaInNameException;
import no.ntnu.idatx2001.wargames.exceptions.FileNotCsvException;
import no.ntnu.idatx2001.wargames.exceptions.InvalidCsvFormatException;
import no.ntnu.idatx2001.wargames.exceptions.UnitTypeException;
import no.ntnu.idatx2001.wargames.objects.Army;
import no.ntnu.idatx2001.wargames.objects.units.Unit;
import no.ntnu.idatx2001.wargames.objects.units.UnitFactory;
import no.ntnu.idatx2001.wargames.objects.units.UnitType;

import java.io.*;
import java.util.*;

/**
 * An object for handling data files. Can be used
 * for writing to a file for saving information, or
 * reading a file for retrieving information.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.2
 */
public class DataController{

    private String fileName;
    private File file;

    /**
     * Constructor for a DataController. Takes the inputted
     * string, and turns it into the name of the file that is
     * being created. If the filename already exists, it creates a
     * new filename with numbers incremented after the original name.
     * Only creates csv files. Creates the directory if it doesn't exist.
     *
     * @param fileName      The name of the file that should be created.
     * @throws IOException  if file can't be created.
     */
    public DataController(String fileName) throws IOException {

        String filePath = "src\\main\\resources\\no\\ntnu\\idatx2001\\wargames\\data\\";
        File filePathFile = new File(filePath);

        if(filePathFile.exists()){
            file = new File(filePath + fileName + ".csv");
            if(file.exists()) {
                int i = 0;
                while (file.exists()) {
                    i++;
                    file = new File(filePath + fileName + i + ".csv");
                }
                this.fileName = fileName + i;
            }else{
                this.fileName = fileName;
            }
        }else{
            filePathFile.mkdir();
            file = new File(filePath + fileName + ".csv");
            if(file.exists()) {
                int i = 0;
                while (file.exists()) {
                    i++;
                    file = new File(filePath + fileName + i + ".csv");
                }
                this.fileName = fileName + i;
            }else{
                this.fileName = fileName;
            }
        }


    }

    /**
     * Constructor for a DataController. Takes the inputted
     * file and checks to see if it is a csv file.
     *
     * @param inputFile                     The file that should be handled
     * @throws FileNotCsvException     If the file is not a csv file
     */
    public DataController(File inputFile) throws FileNotCsvException{
        String inputFileName = inputFile.getName();
        String fileExtension = inputFileName.substring(inputFileName.lastIndexOf('.') + 1);

        if(fileExtension.equals("csv")){
            this.file = inputFile;
            this.fileName = inputFileName;
        }else{
            throw new FileNotCsvException("File is not of file type 'csv'");
        }
    }

    /**
     * Returns the file.
     *
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * Returns the file's name
     *
     * @return name of file
     */
    public String getFileName() {
        return fileName;
    }


    /**
     * Write an army's information into a file. First line in the
     * file is the army's name, with every proceeding line containing
     * information about the units in the army. The unit format is:
     * UnitType,UnitName,UnitHealth
     *
     * @param army          the army that should be written into the file
     * @return              true if writing to the file was successful, false if not
     * @throws IOException  if unable to write to file
     */
    public boolean writeArmyToFile(Army army) throws IOException {

        FileWriter fileWriter = new FileWriter(file);

        ArrayList<String> lineArrayList = new ArrayList<>();
        lineArrayList.add(army.getName());

        for (Unit unit : army.getAllUnits()) {
            lineArrayList.add(unit.getUnitType() + "," + unit.getName() + "," + unit.getHealth());
        }

        String finalString = "";
        for (String line : lineArrayList) {
            finalString += line + "\n";
        }

        fileWriter.write(finalString);
        fileWriter.close();
        return true;
    }

    /**
     * Reads this file's information and creates an army that corresponds
     * to the file. It reads the file line for line and creates new unit
     * objects based on the type specified, and applies the name and health
     * values.
     *
     * @return                                      an army object created from the file's information
     * @throws FileNotFoundException                if the file does not exist in the directory
     * @throws InvalidCsvFormatException            if the information in the file representing the unit's class doesn't exist,
     *                                              or if file is empty
     * @throws IllegalArgumentException             if the unit type in the file does not exist in the enum file UnitType
     * @throws CommaInNameException                 if the army name contains a comma
     */
    public Army readArmyFromFile() throws FileNotFoundException, InvalidCsvFormatException, UnitTypeException, IllegalArgumentException, CommaInNameException{

        ArrayList<String> stringArrayList = new ArrayList<>();
        ArrayList<Unit> unitArrayList = new ArrayList<>();

        if(!file.exists()){
            throw new FileNotFoundException("The file doesnt exist in the directory");
        }

        Scanner reader = new Scanner(file);
        while (reader.hasNextLine()){
            stringArrayList.add(reader.nextLine());
        }
        reader.close();

        UnitFactory unitFactory = new UnitFactory();

        for (int i = 1; i < stringArrayList.size(); i++) {
            String[] elementsInLine = stringArrayList.get(i).split(",");


            try{
                if(elementsInLine.length == 3){
                    UnitType unitType = UnitType.valueOf(elementsInLine[0]);
                    unitArrayList.add(unitFactory.createUnit(unitType,elementsInLine[1],Integer.parseInt(elementsInLine[2])));
                }else{
                    throw new InvalidCsvFormatException("Formatting in file is invalid");
                }

            }catch (NumberFormatException nfe){
                throw new NumberFormatException("Health value is not a number");
            }catch (UnitTypeException ute){
                throw new UnitTypeException("Unit type does not exist");
            }catch (IllegalArgumentException iae){
                throw new IllegalArgumentException("Unit type not specified in enum list");
            }
        }

        if(stringArrayList.isEmpty()){
            throw new InvalidCsvFormatException("File is empty");
        }

        if(stringArrayList.get(0).contains(",")){
            throw new CommaInNameException("Army name cannot contain a comma");
        }

        return new Army(stringArrayList.get(0),unitArrayList);
    }

}
