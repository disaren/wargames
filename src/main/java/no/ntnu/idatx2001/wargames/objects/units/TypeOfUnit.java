package no.ntnu.idatx2001.wargames.objects.units;

/**
 * Interface for getting the type of unit
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
@FunctionalInterface
public interface TypeOfUnit{
    /**
     * Returns the unit type of the specified unit
     *
     * @param u unit that should be checked
     * @return  the unit type of the unit specified
     */
    UnitType getType(Unit u);
}
