package no.ntnu.idatx2001.wargames.objects.units;

import no.ntnu.idatx2001.wargames.exceptions.CommaInNameException;
import no.ntnu.idatx2001.wargames.objects.TerrainType;

/**
 * Represents a unit. A unit is an object
 * containing variables such as health points, attack points
 * and defense points,
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.1.2
 */
public abstract class Unit {
    private String name;
    private int health;
    private int attack;
    private int armor;
    private TerrainType terrainType;

    /**
     * Constructor for a unit.
     *
     * @param name The name of the unit, cannot contain ','
     * @param health Amount of health points, cannot be negative
     * @param attack Amount of attack points
     * @param armor Amount of defense points
     * @throws IllegalArgumentException health is negative or armor is negative
     * @throws CommaInNameException     if name contains a ','
     */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException, CommaInNameException {

        if(!name.contains(",")){
            this.name = name;
        }else{
            throw new CommaInNameException("Name cannot contain a ','");
        }

        if(health >= 0){
            this.health = health;
        }else{
            throw new IllegalArgumentException("Health cannot be negative");
        }

        this.attack = attack;

        if(armor >= 0){
            this.armor = armor;
        }else{
            throw new IllegalArgumentException("Armor cannot be negative");
        }

        terrainType = null;
    }

    /**
     * Attacks an opponent, reducing their health. Their health
     * is reduced by this unit's attack power, and mitigated by their
     * defense points.
     *
     * @param opponent the unit that is going to be attacked
     */
    public void attack(Unit opponent){
        int newHealth = opponent.getHealth() - (this.getAttack() + this.getAttackBonus()) + (opponent.getArmor() + opponent.getResistBonus());
        if(newHealth < 0){
            newHealth = 0;
        }
        opponent.setHealth(newHealth);
    }

    /**
     * Returns the name of the unit
     *
     * @return name of the unit
     */
    public String getName() {
        return name;
    }

    /**
     * Returns amount of health
     *
     * @return health of unit
     */
    public int getHealth() {
        return health;
    }

    /**
     * Returns amount of attack points
     *
     * @return attack points
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Returns amount of armor points
     *
     * @return armor points
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Returns the terrain type that the unit is standing on
     *
     * @return Terrain type of the object TerrainType
     */
    public TerrainType getTerrainType(){
        return terrainType;
    }

    /**
     * Set the amount of health points of this unit
     *
     * @param health the amount of health the unit will have
     * @throws IllegalArgumentException if health given is negative
     */
    public void setHealth(int health) {
        if(health >= 0){
            this.health = health;
        }else{
            throw new IllegalArgumentException("Health cant be negative");
        }
    }

    /**
     * Set the terrain type of the unit to represent what kind of
     * terrain the unit is standing on. Based on the type of unit
     * and terrain, different advantages and disadvantages are calculated
     *
     * @param terrainType               The terraintype the unit should stand on
     * @throws NullPointerException     If parameter is null
     */
    public void setTerrainType(TerrainType terrainType){
        if(!(terrainType == null)){
            this.terrainType = terrainType;
        }else{
            throw new NullPointerException("Terrain type cannot be null");
        }
    }

    /**
     * Returns a standard toString string containing
     * the field variables of this object
     *
     * @return String containing the field variables
     */
    @Override
    public String toString() {
        return this.name + " " +
                "| HP: " + this.health + " " +
                "| Atk: " + this.attack + " " +
                "| Def: " + this.armor;
    }

    /**
     * Returns attack bonus that is specific to
     * this type of unit.
     *
     * @return attack bonus
     */
    public abstract int getAttackBonus();

    /**
     * Returns defense bonus that is specific to
     * this type of unit.
     *
     * @return defense bonus
     */
    public abstract int getResistBonus();

    /**
     * Returns the unit type from the UnitType
     * class
     *
     * @return the unit's type in the form of a UnitType object
     */
    public abstract UnitType getUnitType();


}
