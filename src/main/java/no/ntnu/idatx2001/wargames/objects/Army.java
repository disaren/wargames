package no.ntnu.idatx2001.wargames.objects;

import no.ntnu.idatx2001.wargames.exceptions.CommaInNameException;
import no.ntnu.idatx2001.wargames.objects.units.*;

import java.util.*;

/**
 * Represents an army. An army has a name, and
 * consists of a number of units fighting together.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.1.0
 */
public class Army {
    private String name;
    private List<Unit> units;

    /**
     *  Constructor for an army.
     *
     * @param name                          Name of the army
     * @throws CommaInNameException     if army name contains a ','
     */
    public Army(String name) {
        if(!name.contains(",")){
            this.name = name;
        }else{
            throw new CommaInNameException("Name cannot contain a ','");
        }

        units = new ArrayList<>();
    }

    /**
     *  Constructor for an army.
     *
     * @param name                          Name of the army
     * @param units                         Unit list in the army
     * @throws CommaInNameException     if army name contains a ','
     */
    public Army(String name, List<Unit> units) {
        if(!name.contains(",")){
            this.name = name;
        }else{
            throw new CommaInNameException("Name cannot contain a ','");
        }

        this.units = units;
    }

    /**
     * Returns the name of the army
     *
     * @return name of the army
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a unit to the army.
     *
     * @param unit  Unit that will be added to the army
     */
    public void add(Unit unit){
        this.units.add(unit);
    }

    /**
     * Adds a list of units to the army
     *
     * @param units Unit list that will be added to the army
     */
    public void addAll(List<Unit> units) {
        this.units.addAll(units);
    }

    /**
     * Removes the specified unit from the army
     *
     * @param unit                          Unit that will be removed from the army
     * @throws IllegalArgumentException     if army doesnt contain specified unit
     */
    public void remove(Unit unit){
        if(units.contains(unit)){
            this.units.remove(unit);
        }else{
            throw new IllegalArgumentException("Army doesn't contain the specified unit");
        }
    }

    /**
     * Checks if the army has units.
     *
     * @return true if the army has units, false if it's empty.
     */
    public boolean hasUnits(){
        return !this.units.isEmpty();
    }

    /**
     * Returns a list of all the units that are currently in the army
     *
     * @return List of all the current units in the army
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Returns a random unit from the army.
     *
     * @return random unit from the army
     */
    public Unit getRandom(){
        Random rand = new Random();
        int randomIndex = rand.nextInt(units.size());
        return units.get(randomIndex);
    }

    /**
     * Returns a list of all units of the type Infantry from the unit list
     * @return List of all infantry units in the army
     */
    public List<Unit> getInfantryUnits(){
        ArrayList<Unit> infantryUnits = new ArrayList<>();

        TypeOfUnit infantry = (u1) -> {
            if(u1 instanceof InfantryUnit){
                return UnitType.INFANTRY;
            }else{
                return null;
            }
        };


        for (Unit unit: units) {
            if(infantry.getType(unit) == UnitType.INFANTRY){
                infantryUnits.add(unit);
            }
        }

        return infantryUnits;
    }

    /**
     * Returns a list of all units of the type Cavalry from the unit list
     * @return List of all Cavalry units in the army
     */
    public List<Unit> getCavalryUnits(){
        ArrayList<Unit> cavalryUnits = new ArrayList<>();

        TypeOfUnit cavalry = (u1) -> {
            if(u1 instanceof CavalryUnit){
                return UnitType.CAVALRY;
            }else{
                return null;
            }
        };

        for (Unit unit: units) {
            if(cavalry.getType(unit) == UnitType.CAVALRY){
                cavalryUnits.add(unit);
            }
        }
        return cavalryUnits;
    }

    /**
     * Returns a list of all units of the type Ranged from the unit list
     * @return List of all ranged units in the army
     */
    public List<Unit> getRangedUnits() {
        ArrayList<Unit> rangedUnits = new ArrayList<>();

        TypeOfUnit ranged = (u1) -> {
            if(u1 instanceof RangedUnit){
                return UnitType.RANGED;
            }else{
                return null;
            }
        };

        for (Unit unit: units) {
            if(ranged.getType(unit) == UnitType.RANGED){
                rangedUnits.add(unit);
            }
        }

        return rangedUnits;

    }

    /**
     * Set the name of the army
     *
     * @param name  The new name of the army
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns a list of all units of the type Commander from the unit list
     * @return List of all commander units in the army
     */
    public List<Unit> getCommanderUnits(){
        ArrayList<Unit> commanderUnits = new ArrayList<>();

        TypeOfUnit commander = (u1) -> {
            if(u1 instanceof CommanderUnit){
                return UnitType.COMMANDER;
            }else{
                return null;
            }
        };

        for (Unit unit: units) {
            if(commander.getType(unit) == UnitType.COMMANDER){
                commanderUnits.add(unit);
            }
        }

        return commanderUnits;
    }

    /**
     * Returns a list of all units of the type Mage from the unit list
     *
     * @return List of all commander units in the army
     */
    public List<Unit> getMageUnits(){
        ArrayList<Unit> mageUnits = new ArrayList<>();

        TypeOfUnit commander = (u1) -> {
            if(u1 instanceof CommanderUnit){
                return UnitType.MAGE;
            }else{
                return null;
            }
        };

        for (Unit unit: units) {
            if(commander.getType(unit) == UnitType.MAGE){
                mageUnits.add(unit);
            }
        }

        return mageUnits;
    }

    /**
     * Gets a list of all the units in the army by the unit type specified
     *
     * @param unitType  the unit type the list should only contain
     * @return          a list object containing all the units with the specified unit type
     */
    public List<Unit> getUnitsByType(UnitType unitType){
        ArrayList<Unit> unitArrayListByType = new ArrayList<>();

        TypeOfUnit typeOfUnit = (u1) -> {
            for (UnitType ut: UnitType.values()) {
                if(u1.getUnitType() == ut){
                    return ut;
                }
            }
            return null;
        };

        for (Unit unit: units) {
            if(typeOfUnit.getType(unit) == unitType){
                unitArrayListByType.add(unit);
            }
        }

        return unitArrayListByType;

    }


    /**
     * Returns the name of the army
     *
     * @return String representing the name of the army
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Standard equals method, checks if the specified object
     * is equal to this object
     *
     * @param o     The object that will be checked
     * @return      true if the object is equal to this object, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     *  Standard hash method, uses the name and units
     *
     * @return   Hash, using name and units
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

}
