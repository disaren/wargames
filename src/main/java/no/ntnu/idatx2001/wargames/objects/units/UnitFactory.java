package no.ntnu.idatx2001.wargames.objects.units;

import no.ntnu.idatx2001.wargames.exceptions.UnitTypeException;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a factory for creating units
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class UnitFactory {

    /**
     * A method for creating units based on their
     * type, name and health.
     *
     * @param unitType                      The type of unit that should be created
     * @param name                          The name of the created unit
     * @param health                        The amount of health the created unit will have
     * @return                              A unit based on the type of unit specified
     * @throws UnitTypeException            If the unit type specified doesn't exist as a class
     */
    public Unit createUnit(UnitType unitType, String name, int health) throws UnitTypeException {

        if(unitType == UnitType.INFANTRY){
            return new InfantryUnit(name,health);
        }
        else if(unitType == UnitType.RANGED){
            return new RangedUnit(name,health);
        }
        else if(unitType == UnitType.CAVALRY){
            return new CavalryUnit(name,health);
        }
        else if(unitType == UnitType.COMMANDER){
            return new CommanderUnit(name,health);
        }else if(unitType == UnitType.MAGE){
            return new MageUnit(name,health);
        }
        else{
            throw new UnitTypeException("Specified unit type does not exist as a class");
        }
    }


    /**
     * A method for creating unit lists based on their
     * type, name and health.
     *
     * @param numberOfUnits                 The number of duplicate units that should be in the list
     * @param unitType                      The type of unit that should be created
     * @param name                          The name of the created unit
     * @param health                        The amount of health the created unit will have
     * @return                              A unit list based on the type of unit specified
     * @throws UnitTypeException            If the unit type specified doesn't exist as a class
     */
    public List<Unit> createUnitList(int numberOfUnits,UnitType unitType, String name, int health) throws UnitTypeException {
        ArrayList<Unit> unitArrayList = new ArrayList<>();
        try{
            for (int i = 0; i < numberOfUnits; i++) {
                unitArrayList.add(createUnit(unitType,name,health));
            }
        }catch (UnitTypeException ute){
            throw new UnitTypeException("Specified unit type does not exist as a class");
        }

        return unitArrayList;
    }

}
