package no.ntnu.idatx2001.wargames.objects;

import no.ntnu.idatx2001.wargames.objects.units.Unit;

/**
 * Represents a battle. A battle is a battle
 * between two armies, with units attacking each other
 * until there is one winner.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class Battle {
    private Army armyOne;
    private Army armyTwo;

    private Unit attackerOne;
    private Unit attackerTwo;

    private int sleepTime = 1000;


    /**
     * Constructor for a battle without terrain.
     *
     * @param armyOne       The first army which starts the attack
     * @param armyTwo       The second army which will participate in the battle
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;

        attackerOne = null;
        attackerTwo = null;
    }

    /**
     * Constructor for a battle with terrain.
     *
     * @param armyOne       The first army which starts the attack
     * @param armyTwo       The second army which will participate in the battle
     * @param terrainType   Specify the terrain the battle will happen
     */
    public Battle(Army armyOne, Army armyTwo, TerrainType terrainType) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;

        attackerOne = null;
        attackerTwo = null;

        for (Unit unit: armyOne.getAllUnits()) {
            unit.setTerrainType(terrainType);
        }
        for (Unit unit: armyTwo.getAllUnits()) {
            unit.setTerrainType(terrainType);
        }
    }

    /**
     *  Simulates a battle between two armies. Takes a random unit
     *  from each of the armies starts a skirmish between them. If
     *  a unit's health is reduced to 0, they are removed from the army.
     *  The battle is over once one of the armies don't have any more units.
     *
     * @return                          the winner army of the battle
     * @throws InterruptedException     if the thread is interrupted
     */
    public Army simulate() throws InterruptedException {
        while(armyOne.hasUnits() && armyTwo.hasUnits()){
            Unit unitFromArmyOne = armyOne.getRandom();
            Unit unitFromArmyTwo = armyTwo.getRandom();

            attackerOne = unitFromArmyOne;
            attackerTwo = unitFromArmyTwo;

//            System.out.println("---------------------");

//            System.out.println(unitFromArmyOne + " -- attacked -- " + unitFromArmyTwo);
            armyOne.getRandom().attack(unitFromArmyTwo);
//            System.out.println("After math: " + unitFromArmyTwo);

            if(unitFromArmyTwo.getHealth() == 0){
                armyTwo.remove(unitFromArmyTwo);
            }else{
                if(armyTwo.hasUnits()){
 //                   System.out.println(unitFromArmyTwo + " -- attacked -- " + unitFromArmyOne);
                    armyTwo.getRandom().attack(unitFromArmyOne);
//                    System.out.println("After math: " + unitFromArmyOne);

                    if(unitFromArmyOne.getHealth() == 0){
                        armyOne.remove(unitFromArmyOne);
                    }
                }
            }

            Thread.sleep(sleepTime);
        }


        if(armyOne.hasUnits()){
            return armyOne;
        }else{
            return armyTwo;
        }
    }


    /**
     * Returns the amount of time it takes between each match
     * in the battle
     *
     * @return  integer representing the amount of time between each match
     */
    public int getSleepTime() {
        return sleepTime;
    }

    /**
     * Returns one of the armies in the battle
     *
     * @return  Army object representing one of the armies
     */
    public Army getArmyOne() {
        return armyOne;
    }

    /**
     * Returns one of the armies in the battle
     *
     * @return  Army object representing one of the armies
     */
    public Army getArmyTwo() {
        return armyTwo;
    }

    /**
     * Return one of the attackers in the current match
     *
     * @return  Unit object that is attacking
     */
    public Unit getAttackerOne() {
        return attackerOne;
    }

    /**
     * Return one of the attackers in the current match
     *
     * @return  Unit object that is attacking
     */
    public Unit getAttackerTwo() {
        return attackerTwo;
    }

    /**
     * Set the amount of time between each match
     *
     * @param sleepTime The amount of time between each match,
     *                  1000 = 1 second
     */
    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    /**
     * Returns a standard toString string containing
     * the field variables of this object
     *
     * @return String containing the field variables
     */
    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                '}';
    }
}
