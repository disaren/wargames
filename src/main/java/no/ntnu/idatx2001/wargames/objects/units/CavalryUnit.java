package no.ntnu.idatx2001.wargames.objects.units;

import no.ntnu.idatx2001.wargames.objects.TerrainType;

/**
 * Represents a cavalry unit. Cavalry units excel
 * with the advantage of cavalry, leading to strong
 * charge attacks, but vulnerable at melee combat.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.1.1
 */
public class CavalryUnit extends Unit{
    private boolean charge = true;

    /**
     * Constructor for a cavalry unit.
     *
     * @param name   The name of the unit
     * @param health Amount of health points, input under 0 will be converted to 0
     * @param attack Amount of attack points
     * @param armor  Amount of defense points
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for a cavalry unit. Default values
     * for attack and defense are set to 20 and 12.
     *
     * @param name      The name of the unit.
     * @param health    Amount of health points, input under 0 will be converted to 0
     */
    public CavalryUnit(String name, int health){
        super(name,health,20,12);
    }


    /**
     * Returns attack bonus that is specific to
     * this type of unit. First time this unit
     * attacks, it has a great attack bonus, but
     * gets reduced on the next attacks. If the unit
     * is attacking on plains' terrain, it gets a
     * greater attack bonus
     *
     * @return attack bonus
     */
    @Override
    public int getAttackBonus() {
        if(this.getTerrainType() == TerrainType.PLAINS){
            if(charge){
                charge = false;
                return 12;
            }else{
                return 5;
            }
        }else{
            if(charge){
                charge = false;
                return 8;
            }else{
                return 3;
            }
        }


    }

    /**
     * Returns defense bonus that is specific to
     * this type of unit. If the unit attacks in a
     * forest terrain, it will have no resistance bonus
     *
     * @return defense bonus
     */
    @Override
    public int getResistBonus() {
        if(this.getTerrainType() == TerrainType.FOREST){
            return 0;
        }else{
            return 1;
        }
    }

    /**
     * Returns the unit's type
     *
     * @return UnitType CAVALRY
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.CAVALRY;
    }
}
