package no.ntnu.idatx2001.wargames.objects.units;

import no.ntnu.idatx2001.wargames.objects.TerrainType;

/**
 * Represents a mage unit. A mage unit is
 * a 'glass cannon', which mean they hit hard,
 * but are very fragile. They specialize in
 * range combat
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public class MageUnit extends Unit{

    private final int startHealth;

    /**
     * Constructor for a mage unit.
     *
     * @param name   The name of the unit
     * @param health Amount of health points, input under 0 will be converted to 0
     * @param attack Amount of attack points
     * @param armor  Amount of defense points
     * @throws IllegalArgumentException if name contains a ',' or health is negative
     */
    public MageUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
        this.startHealth = health;
    }

    /**
     * Constructor for a mage unit. Default values
     * for attack and defense are set to 40 and 2.
     *
     * @param name      The name of the unit
     * @param health    Amount of health points, can't go under 0
     */
    public MageUnit(String name, int health){
        super(name,health,40,2);
        this.startHealth = health;
    }

    /**
     * Returns attack bonus that is specific to
     * this type of unit. If the unit attacks from a
     * hill terrain, the attack bonus is greater.
     * If the unit is attacking from a forest terrain,
     * the attack bonus is worse
     *
     * @return attack bonus
     */
    @Override
    public int getAttackBonus() {
        if(this.getTerrainType() == TerrainType.HILL){
            return 15;
        }else if(this.getTerrainType() == TerrainType.FOREST){
            return 2;
        }else{
            return 6;
        }
    }

    /**
     * Returns defense bonus that is specific to
     * this type of unit. The defense bonus varies
     * on the amount of times this unit has been attacked
     * and how much damage that has been inflicted.
     *
     * @return defense bonus
     */
    @Override
    public int getResistBonus() {
        if(2 - (this.startHealth - this.getHealth()) <= 0){
            return 1;
        }else{
            return 2 - (this.startHealth - this.getHealth());
        }
    }

    /**
     * Returns the unit's type
     *
     * @return UnitType RANGED
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.MAGE;
    }
}
