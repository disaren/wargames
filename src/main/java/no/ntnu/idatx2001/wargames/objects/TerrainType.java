package no.ntnu.idatx2001.wargames.objects;

/**
 * Terrain type keywords, representing the different
 * types of terrain that exists
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.0
 */
public enum TerrainType {
    HILL,
    PLAINS,
    FOREST
}
