package no.ntnu.idatx2001.wargames.objects.units;

/**
 * Unit type keywords, representing the different
 * types of units that exists
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.0.2
 */
public enum UnitType {
    INFANTRY,
    RANGED,
    CAVALRY,
    COMMANDER,
    MAGE
}
