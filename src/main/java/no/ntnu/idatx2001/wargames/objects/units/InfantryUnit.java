package no.ntnu.idatx2001.wargames.objects.units;

import no.ntnu.idatx2001.wargames.objects.TerrainType;

/**
 *  Represents an infantry unit. An infantry unit is a unit
 *  which specializes in melee combat, which means it excels
 *  in damage, but often lacks defense.
 *
 * @author Daniel Ireneo Neri Saren
 * @version 1.1.0
 */
public class InfantryUnit extends Unit{

    /**
     * Constructor for an infantry unit.
     *
     * @param name   The name of the unit
     * @param health Amount of health points, can't go under 0
     * @param attack Amount of attack points
     * @param armor  Amount of defense points
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for an infantry unit. Default values
     * for attack and defense are set to 15 and 10.
     *
     * @param name      The name of the unit
     * @param health    Amount of health points, can't go under 0
     */
    public InfantryUnit(String name, int health){
        super(name,health,15,10);
    }

    /**
     * Returns attack bonus that is specific to
     * this type of unit. If the unit is fighting in
     * a forest terrain, the attack bonus is greater
     *
     * @return attack bonus
     */
    @Override
    public int getAttackBonus() {
        if(this.getTerrainType() == TerrainType.FOREST){
            return 6;
        }else{
            return 4;
        }
    }

    /**
     * Returns defense bonus that is specific to
     * this type of unit. If the unit is fighting in
     * a forest terrain, the defense bonus is greater
     *
     * @return defense bonus
     */
    @Override
    public int getResistBonus() {
        if(this.getTerrainType() == TerrainType.FOREST){
            return 4;
        }else{
            return 2;
        }
    }

    /**
     * Returns the unit's type
     *
     * @return UnitType INFANTRY
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.INFANTRY;
    }
}
