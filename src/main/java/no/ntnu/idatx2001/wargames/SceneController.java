package no.ntnu.idatx2001.wargames;

import javafx.stage.Stage;
import no.ntnu.idatx2001.wargames.objects.ListOfArmy;
import no.ntnu.idatx2001.wargames.objects.TerrainType;
import no.ntnu.idatx2001.wargames.scenes.BattleScene;
import no.ntnu.idatx2001.wargames.scenes.PlayScene;
import no.ntnu.idatx2001.wargames.scenes.SimulateScene;
import no.ntnu.idatx2001.wargames.scenes.ManageArmyScene;

/**
 * Represents a scene controller. This class is used
 * for switching the scene for a stage.
 *
 */
public class SceneController {
    private Stage stage;
    private final int BATTLE_SCENE = 1;
    private final int SIMULATE_SCENE = 2;
    private final int MANAGEARMY_SCENE = 3;
    private final int PLAY_SCENE = 9;

    private ListOfArmy listOfArmy;
    private TerrainType terrainType;
    private ListOfArmy selectedListOfArmy;

    /**
     * Constructor for a scene controller
     *
     * @param stage         The stage which will be controlled
     * @param listOfArmy    A list object containing the list of armies in the program
     */
    public SceneController(Stage stage, ListOfArmy listOfArmy){
        this.stage = stage;
        this.listOfArmy = listOfArmy;
        this.terrainType = null;
        this.selectedListOfArmy = null;
    }

    /**
     * Constructor for a scene controller
     *
     * @param stage                 The stage which will be controlled
     * @param listOfArmy            A list object containing the list of armies in the program
     * @param selectedListOfArmy    A list object containing a selection of armies
     * @param terrainType           An object containing a selected terrain
     */
    public SceneController(Stage stage, ListOfArmy listOfArmy, ListOfArmy selectedListOfArmy,TerrainType terrainType){
        this.stage = stage;
        this.listOfArmy = listOfArmy;
        this.terrainType = terrainType;
        this.selectedListOfArmy = selectedListOfArmy;
    }

    /**
     * Creates an instance of a class and switches the scene
     * of this stage to the class' scene
     *
     * @param sceneNumber   number representing the scene that the stage should switch to
     */
    public void switchScenes(int sceneNumber){
        switch (sceneNumber){
            case BATTLE_SCENE -> {
                BattleScene battleScene = new BattleScene(stage, listOfArmy);
                stage.setScene(battleScene.getScene());
                break;
            }
            case SIMULATE_SCENE -> {
                SimulateScene simulateScene = new SimulateScene(stage, listOfArmy, selectedListOfArmy,terrainType);
                stage.setScene(simulateScene.getScene());
                break;
            }
            case MANAGEARMY_SCENE -> {
                ManageArmyScene manageArmyScene = new ManageArmyScene(stage, listOfArmy);
                stage.setScene(manageArmyScene.getScene());
                break;
            }
            case PLAY_SCENE -> {
                PlayScene playScene = new PlayScene(stage, listOfArmy);
                stage.setScene(playScene.getScene());
                break;
            }
        }
    }

}
