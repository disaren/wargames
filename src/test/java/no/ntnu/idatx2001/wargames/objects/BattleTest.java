package no.ntnu.idatx2001.wargames.objects;

import no.ntnu.idatx2001.wargames.objects.units.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {
    Battle battle;
    Army army1;
    Army army2;
    UnitFactory unitFactory;

    @BeforeEach
    void setUp() {
        unitFactory = new UnitFactory();
        ArrayList<Unit> units1 = new ArrayList<>();
        units1.addAll(unitFactory.createUnitList(3,UnitType.INFANTRY,"InfantryArmy1",50));
        units1.addAll(unitFactory.createUnitList(3,UnitType.RANGED,"RangedArmy1",45));

        army1 = new Army("Army1",units1);

        ArrayList<Unit> units2 = new ArrayList<>();
        units2.addAll(unitFactory.createUnitList(3,UnitType.INFANTRY,"InfantryArmy2",50));
        units2.addAll(unitFactory.createUnitList(1,UnitType.RANGED,"RangedArmy2",45));
        units2.addAll(unitFactory.createUnitList(2,UnitType.CAVALRY,"CavalryArmy2",40));

        army2 = new Army("Army2",units2);

        battle = new Battle(army1,army2, TerrainType.HILL);
        battle.setSleepTime(1);
    }

    @Test
    void simulate() throws InterruptedException {
        Army armyWin = battle.simulate();
        System.out.println("\n\n\n\n\n\n");
        System.out.println("WINNER:" + armyWin);
        Assertions.assertTrue(armyWin.equals(army1) || armyWin.equals(army2));
    }
}