package no.ntnu.idatx2001.wargames.objects;

import no.ntnu.idatx2001.wargames.objects.units.Unit;
import no.ntnu.idatx2001.wargames.objects.units.UnitFactory;
import no.ntnu.idatx2001.wargames.objects.units.UnitType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ListOfArmyTest {
    ListOfArmy listOfArmy;


    @BeforeEach
    void setUp() {
        UnitFactory unitFactory = new UnitFactory();
        ArrayList<Army> armyList = new ArrayList<>();
        Army army1 = new Army("Army1",unitFactory.createUnitList(5,UnitType.INFANTRY,"Infantry",50));
        Army army2 = new Army("Army2",unitFactory.createUnitList(5,UnitType.INFANTRY,"Infantry",50));
        Army army3 = new Army("Army3",unitFactory.createUnitList(5,UnitType.INFANTRY,"Infantry",50));
        armyList.add(army1);
        armyList.add(army2);
        armyList.add(army3);

        listOfArmy = new ListOfArmy(armyList);
    }

    @Test
    void getArmyList() {
        List<Army> armyList = listOfArmy.getArmyList();
        Assertions.assertEquals(3,armyList.size());
        Assertions.assertTrue(armyList.get(0).getName().equals("Army1"));
    }

    @Test
    void addArmy() {
        Army testArmy = new Army("TestArmy");
        listOfArmy.addArmy(testArmy);
        Assertions.assertEquals(4,listOfArmy.getArmyList().size());
        Assertions.assertTrue(listOfArmy.getArmyList().contains(testArmy));

        Assertions.assertThrows(IllegalArgumentException.class,() -> listOfArmy.addArmy(testArmy));
    }


    @Test
    void removeArmy() {
        Army testArmy = new Army("TestArmy");
        listOfArmy.addArmy(testArmy);
        listOfArmy.removeArmy(testArmy);
        Assertions.assertTrue(!listOfArmy.getArmyList().contains(testArmy));

        Assertions.assertThrows(IllegalArgumentException.class,() -> listOfArmy.removeArmy(new Army("Fail")));
    }

    @Test
    void removeAllArmies() {
        listOfArmy.removeAllArmies();
        Assertions.assertTrue(listOfArmy.getArmyList().isEmpty());
    }
}