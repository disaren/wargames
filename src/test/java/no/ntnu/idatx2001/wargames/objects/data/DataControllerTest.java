package no.ntnu.idatx2001.wargames.objects.data;

import no.ntnu.idatx2001.wargames.exceptions.FileNotCsvException;
import no.ntnu.idatx2001.wargames.exceptions.InvalidCsvFormatException;
import no.ntnu.idatx2001.wargames.exceptions.UnitTypeException;
import no.ntnu.idatx2001.wargames.objects.Army;
import no.ntnu.idatx2001.wargames.objects.units.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.opentest4j.AssertionFailedError;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;

import static org.junit.jupiter.api.Assertions.*;

class DataControllerTest {
    Army army;
    Army exceptionArmy;
    ArrayList<Unit> units = new ArrayList<>();
    DataController dataController1;
    DataController dataController2;
    DataController dataController3;
    UnitFactory unitFactory;

    DataController readFromFile_InvalidPropertiesFormatException;
    DataController readFromFile_NumberFormatException;
    DataController readFromFile_IllegalArgumentException;


    @BeforeEach
    void setUp() {
        unitFactory = new UnitFactory();

        try {
            dataController1 = new DataController("test");
            dataController2 = new DataController("createTest");
            dataController3 = new DataController("exceptionTest");
            readFromFile_InvalidPropertiesFormatException = new DataController("readFromFile_InvalidPropertiesFormatException");
            readFromFile_NumberFormatException = new DataController("readFromFile_NumberFormatException");
            readFromFile_IllegalArgumentException = new DataController("readFromFile_IllegalArgumentException");

        } catch (IOException e) {
            e.printStackTrace();
        }


        units.addAll(unitFactory.createUnitList(3, UnitType.INFANTRY,"Infantry",50));
        units.addAll(unitFactory.createUnitList(1, UnitType.RANGED,"Ranged",45));

        army = new Army("Army1",units);
        exceptionArmy = new Army("Exception Army");
    }

    @Test
    void getFileName() {
        Assertions.assertEquals("test", dataController1.getFileName());
    }

    @Test
    void writeArmyToFile() {
        try {
            Assertions.assertTrue(dataController2.writeArmyToFile(army));
        } catch (IOException e) {
            Assertions.fail();
            e.printStackTrace();
        }
    }

    @Test
    void readArmyFromFile(){
        try{
            Assertions.assertTrue(dataController2.writeArmyToFile(army));

            Army armyFromFile = dataController2.readArmyFromFile();

            Assertions.assertTrue(army.getName().equals(armyFromFile.getName()));
            for (int i = 0; i < army.getAllUnits().size(); i++) {
                Assertions.assertTrue(army.getAllUnits().get(i).getName().equals(armyFromFile.getAllUnits().get(i).getName()));
                Assertions.assertTrue(army.getAllUnits().get(i).getHealth() == armyFromFile.getAllUnits().get(i).getHealth());
            }

        }catch (IOException | InvalidCsvFormatException | UnitTypeException e){
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void fileDoesntExistException(){
        try{
            Assertions.assertThrows(FileNotFoundException.class,() -> dataController1.readArmyFromFile());
        }catch (Exception e){
            Assertions.fail();
        }
    }

    @Test
    void fileIsEmptyException(){
        try{
            Assertions.assertThrows(NullPointerException.class,() -> dataController3.writeArmyToFile(null));
            Assertions.assertThrows(InvalidCsvFormatException.class,() -> dataController3.readArmyFromFile());
        }catch (Exception e){
            Assertions.fail();
        }
    }

    @Test
    void readFromFile_InvalidPropertiesFormatException(){
        try{


            ArrayList<String> lineArrayList = new ArrayList<>();
            lineArrayList.add("ExceptionFile");
            lineArrayList.add("INFANTRY,UnitName,40");
            lineArrayList.add("RANGED,UnitName");

            FileWriter fileWriterIPF = new FileWriter(readFromFile_InvalidPropertiesFormatException.getFile());

            String finalString = "";
            for (String line : lineArrayList) {
                finalString += line + "\n";
            }

            fileWriterIPF.write(finalString);
            fileWriterIPF.close();

            Assertions.assertThrows(InvalidCsvFormatException.class,() -> {
                Army army = readFromFile_InvalidPropertiesFormatException.readArmyFromFile();
            });
        }catch (IOException ioe){
              Assertions.fail();
        }

        }

    @Test
    void readFromFile_NumberFormatException() throws IOException {
        try{


            ArrayList<String> lineArrayList = new ArrayList<>();
            lineArrayList.add("ExceptionFile");
            lineArrayList.add("INFANTRY,UnitName,40");
            lineArrayList.add("RANGED,UnitName,hello");


            FileWriter fileWriterNFE = new FileWriter(readFromFile_NumberFormatException.getFile());

            String finalString = "";
            for (String line : lineArrayList) {
                finalString += line + "\n";
            }

            fileWriterNFE.write(finalString);
            fileWriterNFE.close();

            Assertions.assertThrows(NumberFormatException.class,() -> {
                Army army = readFromFile_NumberFormatException.readArmyFromFile();
            });
        }catch (IOException ioe){
            Assertions.fail();
        }

    }

    @Test
    void readFromFile_IllegalArgumentException() throws IOException {
        try{


            ArrayList<String> lineArrayList = new ArrayList<>();
            lineArrayList.add("ExceptionFile");
            lineArrayList.add("INFANTRY,UnitName,40");
            lineArrayList.add("hello,UnitName,40");


            FileWriter fileWriterIAE = new FileWriter(readFromFile_IllegalArgumentException.getFile());

            String finalString = "";
            for (String line : lineArrayList) {
                finalString += line + "\n";
            }

            fileWriterIAE.write(finalString);
            fileWriterIAE.close();

            Assertions.assertThrows(IllegalArgumentException.class,() -> {
                Army army = readFromFile_IllegalArgumentException.readArmyFromFile();
            });
        }catch (IOException ioe){
            Assertions.fail();
        }

    }

    @Test
    void inputFileInConstructor() throws FileNotCsvException {
        File file = new File("src\\main\\resources\\no\\ntnu\\idatx2001\\wargames\\data\\inputFileTest.csv");
        DataController dc1 = new DataController(file);

        Assertions.assertTrue(dc1.getFile().equals(file));
        Assertions.assertEquals("inputFileTest.csv",file.getName());

        File fileNotCsv = new File("src\\main\\resources\\no\\ntnu\\idatx2001\\wargames\\data\\inputFileTest.txt");
        Assertions.assertThrows(FileNotCsvException.class,() -> {DataController dc2 = new DataController(fileNotCsv);});

        File nullFile = null;
        Assertions.assertThrows(NullPointerException.class,() -> {DataController dc3 = new DataController(nullFile);});
    }

}