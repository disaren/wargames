package no.ntnu.idatx2001.wargames.objects;

import no.ntnu.idatx2001.wargames.exceptions.CommaInNameException;
import no.ntnu.idatx2001.wargames.objects.units.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {
    Army army;
    ArrayList<Unit> units = new ArrayList<>();
    UnitFactory unitFactory;
    InfantryUnit testInfantry = new InfantryUnit("TestInfantry",50);

    @BeforeEach
    void setUp() {
        unitFactory = new UnitFactory();
        units.addAll(unitFactory.createUnitList(3,UnitType.INFANTRY,"Infantry",50));
        units.add(unitFactory.createUnit(UnitType.RANGED,"Ranged",45));
        units.add(testInfantry);

        army = new Army("Army1",units);
    }

    @Test
    void getName() {
        Assertions.assertEquals("Army1",army.getName());
    }

    @Test
    void add() {
        InfantryUnit infantryUnit = (InfantryUnit) unitFactory.createUnit(UnitType.INFANTRY,"Infantry",50);
        army.add(infantryUnit);
        Assertions.assertTrue(army.getAllUnits().contains(infantryUnit));
    }

    @Test
    void addAll() {
        int numberOfUnits = 2;
        int beforeSize = army.getAllUnits().size();
        army.addAll(unitFactory.createUnitList(numberOfUnits,UnitType.INFANTRY,"Infantry",50));

        Assertions.assertEquals(beforeSize+numberOfUnits,army.getAllUnits().size());
    }

    @Test
    void remove() {
        army.remove(testInfantry);
        Assertions.assertFalse(army.getAllUnits().contains(testInfantry));
    }

    @Test
    void hasUnits() {
        Assertions.assertTrue(army.hasUnits());
    }

    @Test
    void getAllUnits(){
        List<Unit> unitsInArmy = army.getAllUnits();
        Assertions.assertEquals(5,unitsInArmy.size());
    }

    @Test
    void getInfantryUnits(){
        List<Unit> infantryUnits = army.getInfantryUnits();
        Assertions.assertEquals(4,infantryUnits.size());
    }

    @Test
    void getRangedUnits(){
        List<Unit> rangedUnits = army.getRangedUnits();
        Assertions.assertEquals(1,rangedUnits.size());
    }

    @Test
    void getCavalryUnits(){
        List<Unit> cavalryUnits = army.getCavalryUnits();
        Assertions.assertEquals(0,cavalryUnits.size());
    }

    @Test
    void getUnitsByType(){
        List<Unit> infantryUnits = army.getUnitsByType(UnitType.INFANTRY);
        Assertions.assertEquals(4,infantryUnits.size());
    }


    @Test
    void whenExceptionThrown_thenAssertionSucceeds(){
        Assertions.assertThrows(IllegalArgumentException.class,() -> {
           Army exceptionArmy = new Army("failArmy");
           exceptionArmy.remove(unitFactory.createUnit(UnitType.INFANTRY,"fail",10));
        });

        Assertions.assertThrows(NullPointerException.class,() -> new Army(null));
        Assertions.assertThrows(NullPointerException.class,() -> new Army(null, new ArrayList<Unit>()));
        Assertions.assertThrows(CommaInNameException.class,() -> new Army("This Name has a , in it"));
        Assertions.assertThrows(CommaInNameException.class,() -> army.addAll(unitFactory.createUnitList(
                3,UnitType.INFANTRY,"Test,",50)));
    }
}