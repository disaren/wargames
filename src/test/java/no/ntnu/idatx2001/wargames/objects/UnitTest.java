package no.ntnu.idatx2001.wargames.objects;

import no.ntnu.idatx2001.wargames.exceptions.CommaInNameException;
import no.ntnu.idatx2001.wargames.exceptions.UnitTypeException;
import no.ntnu.idatx2001.wargames.objects.units.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UnitTest {
    InfantryUnit infantryUnit;
    RangedUnit rangedUnit;
    CavalryUnit cavalryUnit;
    CommanderUnit commanderUnit;
    MageUnit mageUnit;
    UnitFactory unitFactory;

    @BeforeEach
    void setUp() {
        unitFactory = new UnitFactory();
        infantryUnit = (InfantryUnit) unitFactory.createUnit(UnitType.INFANTRY,"Infantry",50);
        rangedUnit = (RangedUnit) unitFactory.createUnit(UnitType.RANGED,"Ranged",46);
        cavalryUnit = (CavalryUnit) unitFactory.createUnit(UnitType.CAVALRY,"Cavalry",40);
        commanderUnit = (CommanderUnit) unitFactory.createUnit(UnitType.COMMANDER,"Commander",52);
        mageUnit = (MageUnit) unitFactory.createUnit(UnitType.MAGE,"Mage",30);
    }

    @Test
    void attack(){
        infantryUnit.attack(rangedUnit);
        Assertions.assertEquals(43,rangedUnit.getHealth());
        infantryUnit.attack(rangedUnit);
        Assertions.assertEquals(37,rangedUnit.getHealth());
        infantryUnit.attack(rangedUnit);
        Assertions.assertEquals(27,rangedUnit.getHealth());

        rangedUnit.attack(infantryUnit);
        Assertions.assertEquals(41,infantryUnit.getHealth());

        cavalryUnit.attack(infantryUnit);
        Assertions.assertEquals(25,infantryUnit.getHealth());
        cavalryUnit.attack(infantryUnit);
        Assertions.assertEquals(14,infantryUnit.getHealth());
    }

    @Test
    void getName() {
        Assertions.assertEquals("Infantry",infantryUnit.getName());
        Assertions.assertEquals("Ranged",rangedUnit.getName());
        Assertions.assertEquals("Cavalry",cavalryUnit.getName());
        Assertions.assertEquals("Commander",commanderUnit.getName());
        Assertions.assertEquals("Mage",mageUnit.getName());
    }

    @Test
    void getHealth() {
        Assertions.assertEquals(50,infantryUnit.getHealth());
        Assertions.assertEquals(46,rangedUnit.getHealth());
        Assertions.assertEquals(40,cavalryUnit.getHealth());
        Assertions.assertEquals(52,commanderUnit.getHealth());
        Assertions.assertEquals(30,mageUnit.getHealth());
    }

    @Test
    void getAttack() {
        Assertions.assertEquals(15,infantryUnit.getAttack());
        Assertions.assertEquals(15,rangedUnit.getAttack());
        Assertions.assertEquals(20,cavalryUnit.getAttack());
        Assertions.assertEquals(25,commanderUnit.getAttack());
        Assertions.assertEquals(40,mageUnit.getAttack());
    }

    @Test
    void getArmor() {
        Assertions.assertEquals(10,infantryUnit.getArmor());
        Assertions.assertEquals(8,rangedUnit.getArmor());
        Assertions.assertEquals(12,cavalryUnit.getArmor());
        Assertions.assertEquals(15,commanderUnit.getArmor());
        Assertions.assertEquals(2,mageUnit.getArmor());
    }

    @Test
    void setHealth(){
        infantryUnit.setHealth(40);
        Assertions.assertEquals(40,infantryUnit.getHealth());
    }

    @Test
    void getAttackBonus(){
        Assertions.assertEquals(8,cavalryUnit.getAttackBonus());
        Assertions.assertEquals(3,cavalryUnit.getAttackBonus());
    }

    @Test
    void getResistBonus(){
        Assertions.assertEquals(8,rangedUnit.getResistBonus());
        infantryUnit.attack(rangedUnit);
        Assertions.assertEquals(5,rangedUnit.getResistBonus());
    }

    @Test
    void getType(){
        Assertions.assertEquals(UnitType.INFANTRY,infantryUnit.getUnitType());
    }

    @Test
    void getAttackBonus_Terrain(){
        rangedUnit.setTerrainType(TerrainType.HILL);
        Assertions.assertEquals(10,rangedUnit.getAttackBonus());
        rangedUnit.setTerrainType(TerrainType.FOREST);
        Assertions.assertEquals(3,rangedUnit.getAttackBonus());
    }

    @Test
    void whenExceptionThrown_thenAssertionSucceeds(){

        Assertions.assertThrows(NullPointerException.class,() -> unitFactory.createUnit(UnitType.INFANTRY,null,10));
        Assertions.assertThrows(CommaInNameException.class,() -> unitFactory.createUnit(UnitType.INFANTRY,"Name has a , in it",10));
        Assertions.assertThrows(IllegalArgumentException.class,() -> unitFactory.createUnit(UnitType.INFANTRY,"Illegal",-5));
        Assertions.assertThrows(IllegalArgumentException.class,() -> infantryUnit.setHealth(-5));
        Assertions.assertThrows(NullPointerException.class,() -> infantryUnit.setTerrainType(null));
        Assertions.assertThrows(IllegalArgumentException.class,() -> infantryUnit.setTerrainType(TerrainType.valueOf("FAIL")));
        Assertions.assertThrows(IllegalArgumentException.class,()-> new InfantryUnit("yo",10,10,-5));
    }
}